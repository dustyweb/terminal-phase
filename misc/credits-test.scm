(use-modules (goblins)
             (goblins actor-lib cell)
             (ice-9 match)
             (terminal-phase credits)
             (terminal-phase game-loop)
             (terminal-phase ncurses-stuff)
             (system repl coop-server)
             (ncurses curses))

(define %actormap (make-actormap))

(define %screen #f)

(define paused? #f)

(define (do-update screen halt!)
  (unless paused?
    (actormap-poke! %actormap cr 'tick))
  (erase %screen)
  (actormap-peek %actormap cr 'render %screen)
  (refresh %screen))

(define (process-input screen input halt!)
  (match input
    (#\q
     (halt!))
    (#\p
     (set! paused? (not paused?)))
    (_ 'no-op)))

(define-syntax-rule (proxy proc args ...)
  (lambda (args ...)
    (proc args ...)))

(define (run-demo . args)
  (set! %screen (screen-setup!))
  ;; (setup-inlaid-screen!)

  (actormap-poke! %actormap tb 'render %screen)

  (run-game-loop (proxy do-update screen halt!)
                 (proxy process-input screen input halt!)
                 #:repl (spawn-coop-repl-server)
                 #:screen %screen))


(define tb (actormap-spawn! %actormap ^text-buffer 10 60))

;; (actormap-poke! %actormap tb 'insert-char #\a %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'insert-char #\a %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'insert-char #\h %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'crlf)
;; (actormap-poke! %actormap tb 'insert-char #\h %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'insert-char #\o %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'insert-char #\o %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'insert-char #\h %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'crlf)
;; (actormap-poke! %actormap tb 'insert-char #\l %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'insert-char #\o %YELLOW-N #:bold? #t)
;; (actormap-poke! %actormap tb 'insert-char #\l %YELLOW-N #:bold? #t)

(define fake-dpr
  (actormap-spawn! %actormap (lambda _ (lambda _ 'no-op))))

(define cr
  (actormap-spawn! %actormap ^credits fake-dpr))



(run-demo)

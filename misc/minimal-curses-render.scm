(use-modules (goblins)
             (ice-9 match)
             (system repl coop-server)
             (terminal-phase game-loop)
             (ncurses curses))

(define %actormap (make-actormap))

(define %screen #f)

(define (do-update screen halt!)
  'TODO)

(define (process-input screen input halt!)
  (match input
    (#\q (halt!))
    (_ 'no-op)))

(define-syntax-rule (proxy proc args ...)
  (lambda (args ...)
    (proc args ...)))

(define (run-demo . args)
  (set! %screen (screen-setup!))
  ;; (setup-inlaid-screen!)
  (run-game-loop (proxy do-update screen halt!)
                 (proxy process-input screen input halt!)
                 #:repl (spawn-coop-repl-server)
                 #:screen %screen))

(run-demo)

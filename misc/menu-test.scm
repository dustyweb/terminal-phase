(use-modules (goblins)
             (goblins actor-lib pushdown)
             (ice-9 match)
             (system repl coop-server)
             (terminal-phase game-loop)
             (terminal-phase main-menu)
             (terminal-phase ncurses-stuff)
             (ncurses curses))

(define %actormap (make-actormap))

(define %screen #f)

(define paused? #f)

(define (do-update screen halt!)
  (unless paused?
    (actormap-poke! %actormap dpr-forwarder 'tick))
  (erase %screen)
  (actormap-peek %actormap dpr-forwarder 'render %screen)
  (refresh %screen))

(define (process-input screen input halt!)
  (match input
    (#\p (set! paused? (not paused?)))
    (_
     (actormap-poke! %actormap dpr-forwarder 'handle-event input))))

(define-syntax-rule (proxy proc args ...)
  (lambda (args ...)
    (proc args ...)))

(define-values (dpr-pushdown dpr-forwarder)
  (actormap-run! %actormap spawn-pushdown-pair))

(define main-menu
  (actormap-spawn! %actormap ^main-menu dpr-pushdown))

(actormap-poke! %actormap dpr-pushdown 'push main-menu)

(define (run-demo . args)
  (set! %screen (screen-setup!))
  ;; (setup-inlaid-screen!)
  (run-game-loop (proxy do-update screen halt!)
                 (proxy process-input screen input halt!)
                 #:repl (spawn-coop-repl-server)
                 #:screen %screen))

(run-demo)

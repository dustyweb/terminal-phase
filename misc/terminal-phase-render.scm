#!/usr/bin/env -S guile -e main -s
!#

(use-modules (goblins)
             (goblins actor-lib cell)
             (ice-9 match)
             (terminal-phase level-tape)
             (terminal-phase level)
             (terminal-phase score)
             (terminal-phase posinfo)
             (terminal-phase ncurses-stuff)
             (system repl coop-server)
             (terminal-phase game-loop))

(use-modules (fibers)
             (goblins)
             (goblins actor-lib ticker)
             (ncurses curses)
             (system repl coop-server)
             (ice-9 match))

(define am (make-actormap))
(define l1-tape
  (open-level-tape "/home/cwebber/devel/terminal-phase/assets/levels/level1.txt"))
(define total-score-tracker (actormap-spawn! am ^total-score-tracker))
(define score-tracker (actormap-poke! am total-score-tracker 'sprout))
(define lives (actormap-spawn! am ^cell 3))
(define level
  (actormap-spawn! am ^level l1-tape score-tracker lives
                   #:starfield? #t))
(define ticker (actormap-peek am level 'get-level-ticker))

(install-colors!)

;; Advance by 3 seconds of gameplay
(define* (tick-level! ticks #:key test-first?)
  (do ((i 0 (1+ i)))
      ((= i ticks))
    ;; ;; Can we safely do this?
    ;; (when test-first?
    ;;   (actormap-run am (lambda ()
    ;;                      ($ level 'tick)
    ;;                      (print-posinfos!
    ;;                       (map (lambda (obj) ($ obj 'posinfo))
    ;;                            ($ ticker 'get-ticked))))))
    (actormap-poke! am level 'tick)))

;; Advance the game

;; (tick-level! (* 30 3))

(define (posinfos)
  (map (lambda (obj) (actormap-peek am obj 'posinfo))
       (actormap-peek am ticker 'get-ticked)))

;; (define (print-posinfo! pinfo)
;;   (define-values (x y char picolor layer bg-color)
;;     (values (posinfo-x pinfo)
;;             (posinfo-y pinfo)
;;             (posinfo-char pinfo)
;;             (posinfo-color pinfo)
;;             (posinfo-layer pinfo)
;;             (posinfo-bg-color pinfo)))
;;   (addch %stdscr (color (match picolor
;;                           ('white %WHITE-N)
;;                           ('red %RED-N)
;;                           ((or 'yellow 'bryellow) %YELLOW-N)
;;                           ('cyan %CYAN-N)
;;                           ('magenta %MAGENTA-N)
;;                           (_ %WHITE-N))
;;                         (bold char))
;;          #:x (+ x 1)
;;          #:y (+ y 1)))

;; (define (print-posinfos! pinfos)
;;   (match pinfos
;;     ((? posinfo? pinfo)
;;      (print-posinfo! pinfo))
;;     ('() 'done)
;;     ((posinfo rest-posinfos ...)
;;      (print-posinfos! posinfo)
;;      (print-posinfos! rest-posinfos))))


(define-syntax-rule (import! id ...)
  (begin (define id
           (@@ (goblins core)
               id))
         ...))

(import! whactormap-data-wht
         _make-actormap
         actormap-data
         whactormap-metatype
         make-whactormap-data
         actormap-vat-connector)

(define (copy-whactormap am)
  "Copy whactormap AM to a new whactormap with the same contents."
  (define old-ht (whactormap-data-wht (actormap-data am)))
  (define new-ht (make-weak-key-hash-table))
  ;; Update new-ht with all of old-ht's values
  (hash-for-each (lambda (key val)
                   (hashq-set! new-ht key val))
                 old-ht)
  ;; Return new ly made whactormap
  (_make-actormap whactormap-metatype
                  (make-whactormap-data new-ht)
                  (actormap-vat-connector am)))

(define-syntax-rule (run body ...)
  (actormap-run! am (lambda () body ...)))



;; stuff we're overriding from minimal-curses-render.scm

;; Well this is closer to the real fps after rendering
;; (timeout! %stdscr 18)

(define (handle-input! input)
  (actormap-poke! am level 'handle-event input))

(define paused? #f)
(define old-state #f)

(define flash-time 45)
(define bright-flash-time 4)

(define (flash-status! new-status)
  (set! status new-status)
  (set! flash-status flash-time))

(define flash-status 0)
(define status "")

(define (process-input screen input halt!)
  (match input
    (#\q
     (halt!))
    (#\n
     (tick-level! 1))
    (#\p
     (if paused?
         (begin (flash-status! "We're back!")
                (set! paused? #f))
         (begin (flash-status! "Paused!")
                (set! paused? #t))))
    (#\t
     (flash-status! "Saved state!")
     (set! old-state (copy-whactormap am)))
    (#\r
     (cond
      (old-state
       (flash-status! "Restored state!")
       (set! am (copy-whactormap old-state)))
      (#t
       (flash-status! "Nothing to restore!"))))
    (258 (handle-input! "<down>"))
    (259 (handle-input! "<up>"))
    (260 (handle-input! "<left>"))
    (261 (handle-input! "<right>"))
    (_ (handle-input! input))))


(define (print-status! screen)
  (cond
   ((> flash-status (- flash-time bright-flash-time))
    (addchstr screen (bold-on status) #:x 4 #:y 0))
   ((> flash-status 0)
    (addstr screen status #:x 4 #:y 0)))
  (when (> flash-status 0)
    (set! flash-status (1- flash-status))))

(define (do-update screen halt!)
  (unless paused?
    (tick-level! 1))
  (erase screen)
  (actormap-peek am level 'render screen)
  (print-status! screen)
  (refresh screen))

(define (proxy proc)
  (define (proxied . args)
    (apply proc args))
  proxied)

(define (main . args)
  (run-game-loop (proxy do-update)
                 (proxy process-input)
                 #:repl (spawn-coop-repl-server)))

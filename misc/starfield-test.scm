(use-modules (goblins)
             (goblins actor-lib cell)
             (ice-9 match)
             (terminal-phase level-tape)
             (terminal-phase level)
             (terminal-phase score)
             (terminal-phase posinfo))

(use-modules (terminal-phase level-const)
             (terminal-phase level)
             (terminal-phase starfield))

(define am (make-actormap))
(define starfield (actormap-spawn! am ^starfield (* LEVEL-WIDTH 2) (* LEVEL-HEIGHT 4)))
(actormap-run! am (lambda _ (prime-starfield starfield (* LEVEL-WIDTH 2))))

(define (do-update)
  (erase %stdscr)
  (actormap-poke! am starfield 'advance 1)
  (actormap-peek am starfield 'render %stdscr
                 #:x-start 0
                 #:y-start 0)
  (refresh %stdscr))

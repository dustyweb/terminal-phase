#!/bin/sh

## This file is waived into the public domain, as-is, no warranty provided.
##
## If the public domain doesn't exist where you live, consider
## this a license which waives all copyright and neighboring intellectual
## restrictions laws mechanisms, to the fullest extent possible by law,
## as-is, no warranty provided.
##
## No attribution is required and you are free to copy-paste and munge
## into your own project.

autoreconf -vif

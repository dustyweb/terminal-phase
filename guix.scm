;; This file is waived into the public domain, as-is, no warranty provided.
;;
;; If the public domain doesn't exist where you live, consider
;; this a license which waives all copyright and neighboring intellectual
;; restrictions laws mechanisms, to the fullest extent possible by law,
;; as-is, no warranty provided.
;;
;; No attribution is required and you are free to copy-paste and munge
;; into your own project.

(use-modules
  (guix gexp)
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix git-download)
  (guix download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo)
  (srfi srfi-1))

(define (keep-file? file stat)
  (not (any (lambda (my-string)
              (string-contains file my-string))
            (list ".git" ".dir-locals.el" "guix.scm"))))

(package
  (name "terminal-phase")
  (version "1.2-pre")
  (source (local-file (dirname (current-filename))
                      #:recursive? #t
                      #:select? keep-file?))
  (build-system gnu-build-system)
  (arguments
   (list #:make-flags
	 #~(list "GUILE_AUTO_COMPILE=0")
	 #:phases
	 #~(modify-phases %standard-phases
	     (add-before 'configure 'autoreconf
	       (lambda _ (invoke "autoreconf" "-vif")))
	     (add-after 'install 'wrap-binaries
	       (lambda _
		 (use-modules (ice-9 ftw))
		 (define (guile-compiled-dir out version)
		   (string-append out "/lib/guile/"
				  version
				  "/site-ccache"))
		 (define (guile-uncompiled-dir out version)
		   (string-append out
				  "/share/guile/site"
				  (if (string-null? version) ""
				      "/")
				  version))
		 (define (guile-dep-path env modules path)
		   (list env ":" 'prefix
			 (cons modules
			       (map (λ (input)
				      (string-append input path))
				    (list #$guile-goblins
					  #$guile-ncurses)))))
		 (define bin (string-append #$output "/bin/"))
		 (define site (guile-uncompiled-dir #$output ""))
		 (define version (caddr (scandir site)))
		 (wrap-program (string-append bin "terminal-phase")
		   (guile-dep-path
		    "GUILE_LOAD_PATH"
		    (guile-uncompiled-dir #$output version)
		    (guile-uncompiled-dir "" version))
		   (guile-dep-path
		    "GUILE_LOAD_COMPILED_PATH"
		    (guile-compiled-dir #$output version)
		    (guile-compiled-dir "" version))))))))
  (native-inputs (list autoconf
		       automake
		       pkg-config
		       texinfo))
  (inputs (list guile-3.0
		guile-goblins
		guile-ncurses))
  #;(propagated-inputs (list ))
  (synopsis "")
  (description "")
  (home-page
    "https://gitlab.com/dustyweb/terminal-phase")
  (license license:gpl3+))


(define-module (terminal-phase)
  #:use-module (goblins)
  #:use-module (goblins core)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib pushdown)
  #:use-module (goblins actor-lib ticker)
  #:use-module (goblins persistence-store syrup)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-37)    ; command line arg parsing
  #:use-module (ncurses curses)
  #:use-module (terminal-phase engine-actions)
  #:use-module (terminal-phase game-loop)
  #:use-module (terminal-phase globals)
  #:use-module (terminal-phase level)
  #:use-module (terminal-phase level-const)
  #:use-module (terminal-phase level-tape)
  #:use-module (terminal-phase score)
  #:use-module (terminal-phase main-menu)
  #:use-module (terminal-phase ncurses-stuff)
  #:use-module (terminal-phase paths)
  #:use-module (terminal-phase posinfo)
  #:use-module (system repl coop-server)
  #:use-module (system repl server)
  #:export (run-terminal-phase
            %actormap))

(define %actormap (make-actormap))

(define-values (dpr-pushdown dpr-forwarder)
  (actormap-run! %actormap spawn-pushdown-pair))
;; Set the parameter
(%dpr-pushdown dpr-pushdown)


(define paused? #f)
(define old-state #f)

(define flash-time 45)
(define bright-flash-time 4)

(define (flash-status! new-status)
  (set! status new-status)
  (set! flash-status flash-time))

(define flash-status 0)
(define status "")

(define %last-input #f)

(define (process-input screen input halt!)
  (set! %last-input input)
  (match input
    (410        ; KEY_RESIZE
     (set! %resize-screen? #t))
    (#\n
     (parameterize ((%dpr-pushdown dpr-pushdown))
       ((if (%debug?)
            actormap-poke!
            actormap-reckless-poke!)
        %actormap dpr-forwarder 'tick)))
    (#\p
     (when (%debug?)
       (if paused?
           (begin (flash-status! "We're back!")
                  (set! paused? #f))
           (begin (flash-status! "Paused!")
                  (set! paused? #t)))))
    (#\t
     (when (%allow-time-travel?)
       (flash-status! "Saved state!"))
     (set! old-state (copy-whactormap %actormap)))
    (#\r
     (when (%allow-time-travel?)
       (cond
        (old-state
         (flash-status! "Restored state!")
         (set! %actormap (copy-whactormap old-state)))
        (#t
         (flash-status! "Nothing to restore!")))))
    (_ ((if (%debug?)
            actormap-poke!
            actormap-reckless-poke!)
        %actormap dpr-forwarder 'handle-event input)))
  (apply-all-engine-actions! %actormap)
  (when (actormap-peek %actormap dpr-pushdown 'empty?)
    (halt!)))

(define (print-status! screen)
  (cond
   ((> flash-status (- flash-time bright-flash-time))
    (addchstr screen (bold-on status) #:x 4 #:y 0))
   ((> flash-status 0)
    (addstr screen status #:x 4 #:y 0)))
  (when (> flash-status 0)
    (set! flash-status (1- flash-status))))

(define (do-update screen halt!)
  (unless paused?
    (actormap-poke! %actormap dpr-forwarder 'tick)
    (apply-all-engine-actions! %actormap))
  (when %resize-screen?
    (setup-inlaid-screen!)
    (set! %resize-screen? #f))
  (erase %inlaid-screen)
  (actormap-peek %actormap dpr-forwarder 'render %inlaid-screen)
  (print-status! %inlaid-screen)
  (refresh %inlaid-screen))

(define-syntax-rule (proxy proc args ...)
  (lambda (args ...)
    (proc args ...)))

(define %screen #f)
(define %inlaid-screen #f)

;; TODO: get screen size from current state object
(define (setup-inlaid-screen!)
  (when %inlaid-screen
    (delwin %inlaid-screen)
    (erase %screen))
  (match (getmaxyx %screen)
    ((rows cols)
     (define wh-pair (actormap-poke! %actormap dpr-forwarder 'screen-size))
     (define width (car wh-pair))
     (define height (cdr wh-pair))
     (set! %inlaid-screen
	   (newwin height width
		   (max (- (floor/ rows 2) (floor/ height 2) 2) 0)
		   (max (- (floor/ cols 2) (floor/ width 2)) 0))))))

(define *help-text*
  "\
Usage: terminal-phase [OPTION]... [LEVEL]...

Terminal Phase: a space shooter that runs in your terminal!

By default opens the usual level set, but you can specify your own
by passing a list of LEVELs you'd like to play.

Options:
  -h, --help              See this help menu.
  -l, --listen[=PORT-OR-SOCKET-FILE]
                          Start a guile repl for debugging listening on
                          PORT-OR-SOCKET-FILE, which if an integer is a
                          port run on localhost, otherwise is a filename
                          for a unix domain socket.
  -S, --no-starfield      Disable the background starfield.
  -t, --time-travel       Turns on time travel keys (t and r).
  -d, --debug             Uses transactions on actormap (slightly slower)
                          and allows for whole-program-level-pausing for
                          better exploring program state.

License: GPLv3 or later, as published by the FSF.
Home page: https://gitlab.com/dustyweb/terminal-phase")


(define (run-terminal-phase args)
  (define repl-server #f)
  (define (integer-string? arg)
    (and arg
         (string-every char-set:digit arg)))
  (define options
    (list
     (option '(#\h "help") #f #f
             (lambda (opt name arg loads)
               (display *help-text*)
               (newline)
               (quit)))
     (option '(#\ "listen") #f #t
             (lambda (opt name arg loads)
               (match arg
                 (#f
                  (set! repl-server
                        (make-tcp-server-socket)))
                 ((? integer-string?)
                  (set! repl-server
                        (make-tcp-server-socket #:port (string->number arg))))
                 ((? string?)
                  (set! repl-server
                        (make-unix-domain-server-socket #:path arg))))
               loads))
     (option '(#\S "no-starfield") #f #f
             (lambda (opt name arg loads)
               (%starfield? #f)
               loads))
     (option '(#\t "time-travel") #f #f
             (lambda (opt name arg loads)
               (%allow-time-travel? #t)
               loads))
     (option '(#\d "debug") #f #f
             (lambda (opt name arg loads)
               (%debug? #t)
               loads))))

  (define levels
    (args-fold (cdr args)
               options
               (lambda (opt name arg loads)
                 (error "Unrecognized option `~A'" name))
               (lambda (op loads) (cons op loads))
               '()))

  ;; If no levels are provided, we start the game with the default level
  ;; set
  (when (null? levels)
    (set! levels #f))

  (define main-menu
    (actormap-spawn! %actormap ^main-menu dpr-pushdown levels))

  (actormap-poke! %actormap dpr-pushdown 'push main-menu)

  (set! %screen (screen-setup!))
  (setup-inlaid-screen!)

  (run-game-loop (proxy do-update screen halt!)
                 (proxy process-input screen input halt!)
                 #:repl (and repl-server
                             (spawn-coop-repl-server repl-server))
                 #:screen %screen))


;;; Old stuff, to pull in as appropriate

(define show-fps?
  (make-parameter #f))

;; ;;; Game infrastructure
;; ;;; ===================

;; (define (make-raart-render-game)
;;   (define prev-width
;;     #f)
;;   (define prev-height
;;     #f)
;;   (define cached-blank
;;     #f)

;;   (λ (dispatcher gw)
;;     ;; Find out what we're going to render and see if a special
;;     ;; background color is specified (otherwise use black)
;;     (define-values (to-display bg-color)
;;       (call-with-values
;;        (λ ()
;;          ($ dispatcher 'render))
;;        (λ (to-display [bg-color 'black])
;;          (values (or to-display (raart:blank 0 0))
;;                  bg-color))))
;;     ;; Get the width and height
;;     (define width
;;       (game-display-cols gw))
;;     (define height
;;       (game-display-rows gw))

;;     ;; Regenerate and cache the blank if necessary
;;     (unless (and cached-blank
;;                  (= width prev-width)
;;                  (= height prev-height))
;;       (set! prev-width width)
;;       (set! prev-height height)
;;       (set! cached-blank
;;             (spaced-blank width height)))

;;     (raart:fg 'white
;;               (raart:bg 'black (center-over cached-blank to-display)))))

;; (define current-renderer
;;   (make-parameter (make-raart-render-game)))

;; ;;; Each column is a single column of current terrain.
;; ;;; They're happend'ed together.

;; (define time-till-snapshot
;;   (make-parameter 30))

;; (define (maybe-frametime-label gw frame-time
;;                                [game-name "> Terminal Phase <"])
;;   (if (show-fps?)
;;       (lux-standard-label game-name frame-time)
;;       game-name))

;; (struct game
;;   (;; actormap of participants
;;    actormap
;;    ;; ring buffer of old actormaps
;;    am-ringbuf
;;    ;; countdown till next serialization
;;    snapshot-countdown
;;    ;; dispatcher of ticks, events
;;    dispatch-stack
;;    ;; pd automata pushdown (used to check if empty)
;;    pushdown
;;    ;; resolution info
;;    display-rows
;;    display-cols)
;;   #:methods gen:word
;;   [(define (word-tick gw)
;;      ;; Actors we'll call
;;      (define dispatch-stack (game-dispatch-stack gw))
;;      ;; Transactionally update actors for this tick.
;;      ;; Either all of this succeeds or nothing succeeds.
;;      (define (update!)
;;        ;; run all objects
;;        ($ dispatch-stack 'tick))
;;      (define am
;;        (game-actormap gw))
;;      (actormap-run! am
;;                     update!)  ; test optimizing with: #:reckless? #t
;;      (define cur-countdown
;;        (game-snapshot-countdown gw))
;;      (when (zero? cur-countdown)
;;        (ring-buffer-insert! (game-am-ringbuf gw)
;;                             (snapshot-whactormap am)))
;;      (struct-copy game gw
;;                   [snapshot-countdown (modulo (sub1 cur-countdown) (time-till-snapshot))]))
;;    (define (word-event gw e)
;;      (match e
;;        ;; update screen resolution
;;        [(screen-size-report rows cols)
;;         (struct-copy game gw
;;                      [display-rows rows]
;;                      [display-cols cols])]

;;        ;; enter time-travelling debugger
;;        ["t"
;;         (cond
;;           [(allow-time-travel?)
;;            (match-define (game
;;                           actormap am-ringbuf
;;                           snapshot-countdown dispatch-stack
;;                           pushdown display-rows display-cols)
;;              gw)
;;            (define snapshots
;;              (list->vector (ring-buffer->fifo-list am-ringbuf)))

;;            (ttdebug-game actormap am-ringbuf
;;                          snapshot-countdown dispatch-stack
;;                          pushdown display-rows display-cols
;;                          snapshots (sub1 (vector-length snapshots)))]
;;           [else gw])]

;;        ;; Give it to the current dispatcher
;;        [_
;;         (define (dispatcher-handle-event!)
;;           ($ (game-dispatch-stack gw) 'handle-event e))
;;         (actormap-run! (game-actormap gw)
;;                        dispatcher-handle-event!)  ; test optimizing with: #:reckless? #t
;;         ;; quit if there's no dispatcher on the stack
;;         (if (actormap-peek (game-actormap gw)
;;                            (game-pushdown gw)
;;                            'empty?)
;;             #f gw)]))
;;    (define (word-output gw)
;;      (define (compose-display)
;;        ((current-renderer) (game-dispatch-stack gw) gw))
;;      (define cols (game-display-cols gw))
;;      (define rows (game-display-rows gw))
;;      (define too-small?
;;        (or (< rows 22)
;;            (< cols 60)))
;;      (cond
;;        [too-small?
;;         (raart:vappend
;;          #:halign 'left
;;          (raart:text "Terminal too small!")
;;          (raart:text "60x22 minimum!"))]
;;        [else
;;         ;; Return the raart to be displayed.
;;         ;; Note that we use actormap-run rather than -run! because
;;         ;; there shouldn't be any applicable side effects.
;;         (actormap-run (game-actormap gw)
;;                       compose-display)]))
;;    (define (word-label gw frame-time)
;;      (maybe-frametime-label gw frame-time))
;;    (define (word-fps gw)
;;      ;; 30?  60???
;;      ;; probably 30 is something terminals can reliably
;;      ;; keep up with...
;;      30.0)
;;    (define (word-return gw)
;;      (void))])

;; (struct ttdebug-game game (snapshots selection)
;;   #:methods gen:word
;;   [(define (word-event gw e)
;;      (match e
;;        ;; update screen resolution
;;        [(screen-size-report rows cols)
;;         (struct-copy ttdebug-game gw
;;                      [display-rows #:parent game rows]
;;                      [display-cols #:parent game cols])]
;;        ["<left>"
;;         (struct-copy ttdebug-game gw
;;                      [selection
;;                       (modulo (sub1 (ttdebug-game-selection gw))
;;                               (vector-length (ttdebug-game-snapshots gw)))])]
;;        ["<right>"
;;         (struct-copy ttdebug-game gw
;;                      [selection
;;                       (modulo (add1 (ttdebug-game-selection gw))
;;                               (vector-length (ttdebug-game-snapshots gw)))])]
;;        ;; back to the game
;;        [(or "q" " " "C-[")
;;         (match-define (ttdebug-game actormap am-ringbuf
;;                                     snapshot-countdown dispatch-stack
;;                                     pushdown display-rows display-cols
;;                                     snapshots selection)
;;           gw)
;;         (game actormap am-ringbuf
;;               snapshot-countdown dispatch-stack
;;               pushdown display-rows display-cols)]
;;        ;; enter
;;        ["C-M"
;;         (match-define (ttdebug-game actormap am-ringbuf
;;                                     snapshot-countdown dispatch-stack
;;                                     pushdown display-rows display-cols
;;                                     snapshots selection)
;;           gw)
;;         (define snapshot
;;           (vector-ref snapshots selection))
;;         (cond
;;           [snapshot
;;            (define new-actormap
;;              (hasheq->whactormap snapshot))
;;            (game new-actormap am-ringbuf snapshot-countdown
;;                  dispatch-stack pushdown display-rows display-cols)]
;;           ;; continue as-is, this isn't a valid selection
;;           [else gw])]
;;        [_ gw]))
;;    (define (word-output gw)
;;      (define cols (game-display-cols gw))
;;      (define rows (game-display-rows gw))
;;      (define too-small?
;;        (or (< rows 22)
;;            (< cols 60)))
;;      (cond
;;        [too-small?
;;         (raart:vappend
;;          #:halign 'left
;;          (raart:text "Terminal too small!")
;;          (raart:text "60x22 minimum!"))]
;;        [else
;;         (match-define (ttdebug-game actormap am-ringbuf
;;                                     snapshot-countdown dispatch-stack
;;                                     pushdown display-rows display-cols
;;                                     snapshots selection)
;;           gw)
;;         (define snapshots-len
;;           (vector-length snapshots))
;;         ;; Render the game at selection, if appropriate.
;;         (define selected-snapshot
;;           (vector-ref snapshots selection))
;;         (define background
;;           (if selected-snapshot
;;               (let ([select-am
;;                      (hasheq->whactormap selected-snapshot)])
;;                 (actormap-run select-am
;;                               (λ ()
;;                                 ((current-renderer)
;;                                  dispatch-stack
;;                                  (game select-am am-ringbuf
;;                                        snapshot-countdown dispatch-stack
;;                                        pushdown display-rows display-cols)))))
;;               (spaced-blank display-cols display-rows)))

;;         (define snapshot-ui
;;           (raart:frame
;;            (raart:vappend
;;             #:halign 'center
;;             (raart:text "Restore Snapshot:")
;;             (raart:table
;;              (list
;;               (for/list ([i snapshots-len])
;;                 (define something-here?
;;                   (vector-ref snapshots i))
;;                 ;; if there's nothing at this position, we need to indicate that
;;                 (if (= i selection)
;;                     (raart:bg (if something-here?
;;                                   'brwhite
;;                                   'brblack)
;;                               (raart:fg 'black (raart:text (number->string i))))
;;                     (raart:fg (if something-here?
;;                                   'brwhite
;;                                   'brblack)
;;                               (raart:text (number->string i))))))))))

;;         ;; center the snapshot ui over the bg
;;         (raart:place-at background 0
;;                         (- (quotient (raart:raart-w background) 2)
;;                            (quotient (raart:raart-w snapshot-ui) 2))
;;                         snapshot-ui)]))
;;    (define (word-label gw frame-time)
;;      (maybe-frametime-label gw frame-time
;;                             ">> Time Travel <<"))
;;    (define (word-tick gw)
;;      gw)])

;; (define (new-game rows cols level-files
;;                   #:game-constructor [game-constructor game])
;;   (define actormap (make-actormap))
;;   (define (make-new-game)
;;     (match-define (list dispatcher-pushdown dispatcher-forwarder)
;;       (spawn-pushdown-pair))
;;     (define main-menu
;;       (spawn ^main-menu dispatcher-pushdown level-files))
;;     ($ dispatcher-pushdown 'push main-menu)

;;     (game-constructor actormap
;;                       (make-ring-buffer 20) (time-till-snapshot)
;;                       dispatcher-forwarder dispatcher-pushdown
;;                       rows cols))
;;   (actormap-run! actormap make-new-game
;;                  #:reckless? #t))


;; (define (display* . things)
;;   (for-each display things)
;;   (flush-output))

;; (define (with-clean-tty proc)
;;   (tty-raw!)
;;   (display* (dec-soft-terminal-reset)
;;             (device-request-screen-size))
;;   #;(match-define (screen-size-report rows columns)
;;     (lex-lcd-input (current-input-port)))
;;   (define proc-result
;;     #;(proc 24 80)
;;     (proc 22 72))
;;   #;(display* (dec-soft-terminal-reset)
;;             (clear-screen/home))
;;   proc-result)

;; (define (start-game level-files
;;                     #:game-constructor [game-constructor game])
;;   #;(with-handlers ([exn:fail?
;;                    (lambda (err)
;;                      #;(display* (dec-soft-terminal-reset)
;;                                  (clear-screen/home))
;;                      #;(display* (clear-screen/home)
;;                                  (dec-reset-margins)
                                 
;;                                  (dec-soft-terminal-reset)
;;                                  (xterm-full-reset)
;;                                  )
;;                      #;(displayln "hi")
;;                      #;((error-display-handler) (exn-message err) err)
;;                      #;(displayln "there"))]))
;;   (with-clean-tty
;;     (lambda (rows cols)
;;       (call-with-chaos
;;        (raart:make-raart)
;;        (lambda ()
;;          (fiat-lux (new-game rows cols level-files
;;                              #:game-constructor game-constructor))))))
;;   (void))

;;; =======================================
;;; time traveling debugger kludges go here
;;; =======================================

;; (module+ main
;;   (define level-files
;;     (command-line
;;      #:once-each
;;      [("-d" "--debug-file")
;;       debug-file-arg
;;       "Print current-error-port to this debug file"
;;       (let ([debug-file (open-output-file debug-file-arg
;;                                           #:exists 'append)])
;;         (current-error-port debug-file))]
;;      [("-f" "--show-fps")
;;       "Show current FPS"
;;       (show-fps? #t)]
;;      [("-t" "--time-travel")
;;       "Allow time travel with 't' key"
;;       (allow-time-travel? #t)]
;;      [("--tt-ticks")
;;       tt-ticks
;;       "Number of ticks per snapshot for --time-travel (default: 30)"
;;       (time-till-snapshot (string->number tt-ticks))]
;;      #:args level-files
;;      level-files))

;;   (start-game (match level-files
;;                 ['() #f]
;;                 [_ level-files])))

(define-module (terminal-phase loopdown)
  #:use-module (ice-9 match)
  #:use-module (goblins core)
  #:use-module (goblins define-actor)
  #:use-module (goblins actor-lib methods)
  #:use-module (terminal-phase namespace-env)
  #:export (^loopdown
            ^countdown
            loopdown-env))

(define-actor (^countdown* bcom steps n)
  (methods
   [(counter) n]
   [(zero?)
    (zero? n)]
   [(sub1)
    (unless (zero? n)
      (bcom (^countdown* bcom steps (1- n))))]
   [(reset)
    (bcom (^countdown* bcom steps steps))]))

(define* (^countdown bcom steps #:optional [start-at steps])
  (spawn ^countdown* steps start-at))

;; counts down, but loops
(define-actor (^loopdown* bcom steps n)
  (methods
   [(counter) n]
   [(zero?)
    (zero? n)]
   [(sub1)
    (if (zero? n)
        (bcom (^loopdown* bcom steps steps))
        (bcom (^loopdown* bcom steps (1- n))))]))

(define* (^loopdown bcom steps #:optional [start-at steps])
  (spawn ^loopdown* steps start-at))

(define-namespace-env loopdown-env
  (terminal-phase loopdown)
  (^countdown ^countdown*)
  (^loopdown ^loopdown*))


#;(module+ test
  (require goblins
           rackunit)
  (define am (make-actormap))
  (define ldown
    (actormap-spawn! am ^loopdown 3))
  (check-equal?
   (actormap-peek am ldown 'counter)
   3)
  (check-equal?
   (actormap-peek am ldown 'zero?)
   #f)
  (actormap-poke! am ldown 'sub1)
  (check-equal?
   (actormap-peek am ldown 'counter)
   2)
  (check-equal?
   (actormap-peek am ldown 'zero?)
   #f)
  (actormap-poke! am ldown 'sub1)
  (check-equal?
   (actormap-peek am ldown 'counter)
   1)
  (check-equal?
   (actormap-peek am ldown 'zero?)
   #f)
  (actormap-poke! am ldown 'sub1)
  (check-equal?
   (actormap-peek am ldown 'counter)
   0)
  (check-equal?
   (actormap-peek am ldown 'zero?)
   #t)
  (actormap-poke! am ldown 'sub1)
  (check-equal?
   (actormap-peek am ldown 'counter)
   3)
  (check-equal?
   (actormap-peek am ldown 'zero?)
   #f)

  (define cdown
    (actormap-spawn! am ^countdown 2))
  (check-equal?
   (actormap-peek am cdown 'counter)
   2)
  (actormap-poke! am cdown 'sub1)
  (check-equal?
   (actormap-peek am cdown 'counter)
   1)
  (actormap-poke! am cdown 'sub1)
  (check-equal?
   (actormap-peek am cdown 'counter)
   0)
  (actormap-poke! am cdown 'sub1)
  (check-equal?
   (actormap-peek am cdown 'counter)
   0)
  (actormap-poke! am cdown 'reset)
  (check-equal?
   (actormap-peek am cdown 'counter)
   2)

  (define ldown-proc
    (actormap-spawn! am ^loopdown
                     (let ([n 1])
                       (lambda ()
                         (let ([cur-n n])
                           (set! n (add1 n))
                           cur-n)))))
  
  (check-equal?
   (actormap-peek am ldown-proc 'counter)
   1)
  (check-equal?
   (actormap-peek am ldown-proc 'zero?)
   #f)
  (actormap-poke! am ldown-proc 'sub1)
  (check-equal?
   (actormap-peek am ldown-proc 'counter)
   0)
  (check-equal?
   (actormap-peek am ldown-proc 'zero?)
   #t)
  (actormap-poke! am ldown-proc 'sub1)
  (check-equal?
   (actormap-peek am ldown-proc 'counter)
   2)
  (check-equal?
   (actormap-peek am ldown-proc 'zero?)
   #f)
  (actormap-poke! am ldown-proc 'sub1)
  (check-equal?
   (actormap-peek am ldown-proc 'counter)
   1)
  (check-equal?
   (actormap-peek am ldown-proc 'zero?)
   #f)
  (actormap-poke! am ldown-proc 'sub1)
  (check-equal?
   (actormap-peek am ldown-proc 'counter)
   0)
  (check-equal?
   (actormap-peek am ldown-proc 'zero?)
   #t)
  (actormap-poke! am ldown-proc 'sub1)
  (check-equal?
   (actormap-peek am ldown-proc 'counter)
   3)
  (check-equal?
   (actormap-peek am ldown-proc 'zero?)
   #f))

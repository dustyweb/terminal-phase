(define-module (terminal-phase namespace-env)
  #:use-module (goblins)
  #:export (namespace-env define-namespace-env))

(define-syntax namespace-env-item
  (syntax-rules ()
    ((_ ns (name val))
     (list (list 'ns 'name) val))
    ((_ ns name-val)
     (list (list 'ns 'name-val) name-val))))

(define-syntax namespace-env
  (syntax-rules ()
    ((namespace-env ns item ... #:extends extends)
     (make-persistence-env
      (list (namespace-env-item ns item) ...)
      #:extends extends))
    ((namespace-env ns item ...)
     (make-persistence-env
      (list (namespace-env-item ns item) ...)))))

(define-syntax-rule (define-namespace-env id ns-env-args ...)
  (define id (namespace-env ns-env-args ...)))

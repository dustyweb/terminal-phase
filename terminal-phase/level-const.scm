(define-module (terminal-phase level-const)
  #:export (LEVEL-WIDTH
            LEVEL-HEIGHT
            HALF-LEVEL-WIDTH
            HALF-LEVEL-HEIGHT
            oob?
            default-terrain-speed
            terrain-halted?
            terrain-screaming?
            terrain-very-vast?
            terrain-fast?
            terrain-normal?
            terrain-slow?))

(define LEVEL-WIDTH 50)
(define LEVEL-HEIGHT 13)
(define HALF-LEVEL-WIDTH (floor-quotient LEVEL-WIDTH 2))
(define HALF-LEVEL-HEIGHT (floor-quotient LEVEL-HEIGHT 2))


(define (oob? x y)
  (or (>= x LEVEL-WIDTH)
      (< x 0)
      (>= y LEVEL-HEIGHT)
      (< y 0)))

(define default-terrain-speed
  6)

(define (terrain-halted? speed)
  (not speed))

(define (terrain-screaming? speed)
  (<= speed 2))

(define (terrain-very-fast? speed)
  (<= speed 4))

(define (terrain-fast? speed)
  (< speed 6))

(define (terrain-normal? speed)
  (eqv? speed 6))

(define (terrain-slow? speed)
  (> speed 6))

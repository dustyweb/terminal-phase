(define-module (terminal-phase globals)
  #:export (%resize-screen?
            %starfield?
            %allow-time-travel?
            %dpr-pushdown
            %debug?))

;; Temporal, indicates that a screen resize happened in this tick
(define %resize-screen? #f)

;; Whether or not we want the starfield on in general
(define %starfield?
  (make-parameter #t))
(define %allow-time-travel?
  (make-parameter #f))
;; Allows for "program-level pausing", etc
(define %debug?
  (make-parameter #f))

(define %dpr-pushdown
  (make-parameter #f))

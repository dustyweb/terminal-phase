(define-module (terminal-phase main-menu)
  #:use-module (ice-9 match)
  #:use-module (ncurses curses)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins persistence-store syrup)
  #:use-module (terminal-phase border)
  #:use-module (terminal-phase credits)
  #:use-module (terminal-phase engine-actions)
  #:use-module (terminal-phase globals)
  #:use-module (terminal-phase level)
  #:use-module (terminal-phase level-const)
  #:use-module (terminal-phase level-tape)
  #:use-module (terminal-phase logo)
  #:use-module (terminal-phase ncurses-stuff)
  #:use-module (terminal-phase no-op)
  #:use-module (terminal-phase paths)
  #:use-module (terminal-phase score)
  #:use-module (terminal-phase say-it)
  #:use-module (terminal-phase starfield)
  #:use-module (terminal-phase contrib simple-iterators)
  #:export (^main-menu))

(define fns/ file-name-separator-string)

;; Let's set up some constant values relative to the main menu stuff
(define menu-screen-width (+ LEVEL-WIDTH 12))  ; why +4?  A little unclear on that one
(define menu-screen-height (+ LEVEL-HEIGHT 9))
(define menu-screen-wh-pair (cons menu-screen-width menu-screen-height))

(define* (^main-menu bcom dpr-pushdown #:optional [level-files #f])
  ;; be resumed.  But we start out not knowing.  This allows us
  ;; to clear the status when a game starts that way we can check
  ;; again after.
  (define resume-option? (save-file-exists?))

  (define starfield
    (and (%starfield?)
         (spawn ^starfield (* menu-screen-width 2) (* menu-screen-height 4))))

  (define total-score
    (spawn ^total-score-tracker))

  (define (push-new-main-menu)
    ;; On playing we push a temporary version of a main-menu-to-be to
    ;; replace ourselves so we can check if there's a save file later
    ;; etc
    ($ dpr-pushdown 'pop)
    ($ dpr-pushdown 'push
       (spawn ^make-new-main-menu dpr-pushdown level-files)))

  (define (start-game)
    (define levels
      (match level-files
        [(? pair?)
         (list->tape-eaters level-files)]
        [_ (list->tape-eaters
            (map (λ (fname)
                   (string-append (get-levels-dir) fns/ fname))
                 '("level1.txt" "level2.txt" "level3.txt")))]))
    (define level-manager
      (spawn ^level-manager levels
             ($ total-score 'sprout)))
    (push-new-main-menu)
    ;; Finally push on the level manager
    ($ dpr-pushdown 'push level-manager))

  (define (resume-game)
    (push-new-main-menu)
    (push-engine-action!
     (lambda (am)
       (define syrup-store (make-syrup-store (get-save-file-path)))
       (define restored-game
         (actormap-restore-from-store! am level-env syrup-store))
       (actormap-poke! am dpr-pushdown 'push restored-game))))

  (define (show-help)
    ($ dpr-pushdown 'spawn-push ^help-menu dpr-pushdown))

  (define (show-credits)
    ($ dpr-pushdown 'push (spawn ^credits)))

  (define (quit)
    ($ dpr-pushdown 'pop))

  (define menu-options
    (if resume-option?
        `#(("Play game!" ,start-game)
           ("Resume game!" ,resume-game)
           ("Help" ,show-help)
           ("Show Credits" ,show-credits)
           ("Quit" ,quit))
        `#(("Play game!" ,start-game)
           ("Help" ,show-help)
           ("Show Credits" ,show-credits)
           ("Quit" ,quit))))

  (define menu-options-len
    (vector-length menu-options))

  ;; menu here includes border
  (define menu-width
    (+ (apply max (map (lambda (item)
                         (string-length (car item)))
                       (vector->list menu-options)))
       9))
  (define menu-height
    (+ menu-options-len 4))

  (define logo-offset-x
    (+ (floor/ (- menu-screen-width logo-width) 2) 2))
  (define logo-offset-y 1)

  (define menu-offset-x
    (floor/ (- menu-screen-width menu-width) 2))
  (define menu-offset-y
    (+ logo-offset-y logo-height 1))

  (define* (next #:optional [selection 0])
    (methods
     [(tick)
      (when starfield
        ($ starfield 'advance 2))]
     [(screen-size) menu-screen-wh-pair]
     [(handle-event evt)
      (match evt
        [259				; <up>
         (bcom (next (modulo (1- selection)
                             (vector-length menu-options))))]
        [258				; <down>
         (bcom (next (modulo (1+ selection)
                             (vector-length menu-options))))]
        [(or #\space #\newline)
	 (set! %resize-screen? #t)
         (match (vector-ref menu-options selection)
           [(_option-text option-func)
            (option-func)])]
        [(or #\q #\esc)
         ($ dpr-pushdown 'pop)]
        [_
         'no-op])]
     [(render screen)
      ;; (blit-border screen #:width 56 #:height 19)

      (when starfield
        ($ starfield 'render screen #:x-start 0 #:y-start 0))

      (do ((lines logo-lines (cdr lines))
           (y logo-offset-y (1+ y)))
          ((null? lines))
        (let* ((line (car lines))
               (line-len (string-length line)))
          (do ((i 0 (1+ i))
               (x logo-offset-x (1+ x)))
              ((= i line-len))
            (match (string-ref line i)
              (#\space 'no-op)
              (#\# (addch screen (bold #\space) #:x x #:y y))
              (char (addch screen (bold char) #:x x #:y y))))))

      (blit-border screen
                   #:x menu-offset-x #:y menu-offset-y
                   #:width menu-width #:height menu-height)

      (do ((menu-i 0 (1+ menu-i))
           (y (+ menu-offset-y 2) (1+ y)))
          ((= menu-i menu-options-len))
        (let ((item-text (car (vector-ref menu-options menu-i))))
          (cond
           ((= menu-i selection)
            (addchstr screen
                      (bold (color %YELLOW-N
                                   (format #f "=> ~a" item-text)))
                      #:x (+ menu-offset-x 2)
                      #:y y)
            )
           (else
            (addchstr screen
                      (bold (color %WHITE-N
                                   (format #f "   ~a" item-text)))
                      #:x (+ menu-offset-x 2)
                      #:y y)))))

      ;; (define menu-items
      ;;   (for/map ([option menu-options]
      ;;              [i (in-naturals)])
      ;;     (match option
      ;;       [(list option-text _option-func)
      ;;        (if (= i selection)
      ;;            (raart:fg 'bryellow
      ;;                      (raart:text
      ;;                       (string-append ">> "
      ;;                                      option-text
      ;;                                      " <<")))
      ;;            (raart:fg 'white
      ;;                      (raart:text option-text)))])))
      ;; (raart:vappend
      ;;  #:halign 'center
      ;;  (raart:frame (raart:inset 1 0 (raart:fg 'brwhite logo-raart)))
      ;;  (raart:blank 0 3)
      ;;  (apply
      ;;   raart:vappend
      ;;   #:halign 'center
      ;;   menu-items))
      ]
     ))

  (when starfield
    (prime-starfield starfield 54))

  (next))

;; We push this on so that there can be a fresh state main menu when
;; a game completes.
(define (^make-new-main-menu bcom dpr-pushdown level-files)
  (lambda args
    (define new-main-menu
      (spawn ^main-menu dpr-pushdown level-files))
    ($ dpr-pushdown 'pop)
    ($ dpr-pushdown 'push new-main-menu)
    (apply $ new-main-menu args)))


(define say-it-text
  '("   Arrow keys: move "
    "     Space: fire "
    ""
    " ... that's it! Good luck! "))

(define (^help-menu bcom main-menu dpr-pushdown)
  (methods
   [(tick)
    ($ main-menu 'tick)]
   [(screen-size) menu-screen-wh-pair]
   [(handle-event evt)
    ($ dpr-pushdown 'pop)]
   [(render screen)
    (say-it-to-screen screen say-it-text
                      menu-screen-width menu-screen-height)]
   #;[(render)
    (raart:frame
     (raart:vappend
      #:halign 'center
      (raart:text " Arrow keys: move ")
      (raart:text " Space: fire")
      (raart:blank 0 1)
      (raart:text " ... that's it!  Good luck!")))
   ]
   ))

(define-module (terminal-phase starfield)
  #:use-module (terminal-phase braille-rast)
  #:use-module (terminal-phase namespace-env)
  #:use-module (terminal-phase randrange)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib cell)
  #:use-module (srfi srfi-1)      ; list utils
  #:use-module (srfi srfi-9)      ; records
  #:use-module (srfi srfi-9 gnu)  ; records+
  #:use-module (ncurses curses)
  #:export (^starfield
            prime-starfield
            starfield-env))

;; Periods make perfectly fine starfields, it appears...

;;    .        .
;;  .     .    
;;     .     .     .
;;   .    .    .    
;; .    .        .

;; Hm, only for the intro.  It won't work for the game because what
;; will make the stars look nice is that they move at a different
;; speed that doesn't look like the objects I think.

;; What we really ought to use is the unicode braille characters.
;; So that's what we do.

;; Note that this makes the wireworld proposal of using periods messier.
;; This may mean that we should move to something else like single/double
;; quotes, like so:

;; '''   "'
;;   '   '
;;   '@*''

(define-immutable-record-type <star>
  (make-star x y relative-speed on? blink-time)
  star?
  (x star-x set-star-x)
  (y star-y set-star-y)
  (relative-speed star-relative-speed set-star-relative-speed)
  (on? star-on? set-star-on)
  (blink-time star-blink-time set-star-blink-time))

;; Note that we don't actually bother to restore the starfield with
;; all its current star-state.  It doesn't actually matter!  When
;; the game resumes we can simply start up the new star state all
;; over again.  Curiously enough the default behavior of define-actor
;; does exactly the right thing here, since we ignore all the state
;; defined within the next closure and just use the constructor
;; arguments!
(define-actor (^starfield bcom width height
                          #:key
                          [till-spawn-min 0.3]
                          [till-spawn-max 2.4])
  (define (oob? x)
    (<= x 0))

  (define (new-blink-on-time)
    (* (randrange 10 200) .1))
  (define (new-blink-off-time)
    (* (randrange 300 1000) .1))

  (define (make-new-star initial-distance)
    (define y
      (random height))
    (define relative-speed
      (+ (* (- (randrange 1 15) 10) .1) 1.0))
    (define on?
      (zero? (random 4)))
    (define blink-time
      (if on?
          (new-blink-off-time)
          (new-blink-on-time)))
    (make-star (- width initial-distance) y
               relative-speed on? blink-time))

  (define (new-till-spawn)
    (randrange 5 30))

  (define (next till-spawn stars)
    (methods
     [(advance speed)
      (define base-distance
        (/ 1.0 (+ speed .1)))
      (define new-stars
        (fold
         (lambda (this-star new-stars)
           (define star-distance
             (* base-distance (star-relative-speed this-star)))
           (define new-x
             (- (star-x this-star) star-distance))
           (cond
            ;; drop this star
            [(oob? new-x)
             new-stars]
            ;; Otherwise, we'll add it back having updated some
            ;; properties
            [else
             (let* ((blink-time
                     (- (star-blink-time this-star) star-distance))
                    (on? (star-on? this-star)))
               (when (<= blink-time 0)
                 (set! on? (not on?))
                 (set! blink-time
                       (if on?
                           (new-blink-off-time)
                           (new-blink-on-time))))
               (let ((new-star
                      (set-fields this-star
                                  ((star-x) new-x)
                                  ((star-on?) on?)
                                  ((star-blink-time) blink-time))))
                 (cons new-star new-stars)))]))
         '()
         stars))

      (define next-till-spawn
        (- till-spawn base-distance))
      (when (<= next-till-spawn 0)
        (set! new-stars (cons (make-new-star base-distance) new-stars))
        (set! next-till-spawn (new-till-spawn)))

      (bcom (next next-till-spawn new-stars))]
     [(render to-win #:key (x-start 0) (y-start 0))
      (define grid
        (make-vector height #f))
      (do ((i 0 (1+ i)))
          ((= i height))
        (vector-set! grid i (make-vector width #f)))
      (for-each (lambda (star)
                  (when (star-on? star)
                    (let ((y (star-y star))
                          (x (inexact->exact (floor (star-x star)))))
                      (vector-set! (vector-ref grid (star-y star))
                                   x 'SS))))
                stars)
      (define braille-lines
        (brailleify-grid/list grid))

      (do ((y y-start (1+ y))
           (lines braille-lines (cdr lines)))
          ((null? lines))
        (addstr to-win (car lines) #:x x-start #:y y))]))
  (next (new-till-spawn) '()))

(define (prime-starfield starfield width)
  (do ((i (* width 5) (1- i)))
      ((= i 0))
    ($ starfield 'advance 1.0)))

(define-namespace-env starfield-env
  (terminal-phase starfield)
  ^starfield
  #:extends (list cell-env))

#;(module+ main
  (define am
    (make-actormap))
  (define starfield
    (actormap-spawn! am ^starfield 100 45))
  (actormap-run!
   am (λ () (prime-starfield starfield 30)))
  (raart:draw-here (actormap-peek am starfield 'render))
  )

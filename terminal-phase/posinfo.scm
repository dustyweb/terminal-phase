(define-module (terminal-phase posinfo)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:export (posinfo
            posinfo?
            posinfo-x posinfo-y
            posinfo-char posinfo-color
            posinfo-layer
            posinfo-bg-color
            recolor-posinfo))

;;; Position/rendering/collision info
;;; =================================

#;(define-record-type <posinfo>
  (_make-posinfo x y char color layer bg-color)
  posinfo?
  ;; x & y coords
  (x posinfo-x)
  (y posinfo-y)
  ;; character and color
  (char posinfo-char)
  (color posinfo-color)
  ;; used both for rendering-order and collision category
  (layer posinfo-layer)
  ;; background color (or #f)
  (bg-color posinfo-bg-color))

(define* (posinfo x y char color layer #:optional [bg-color #f])
  #;(_make-posinfo x y char color layer bg-color)
  (vector 'posinfo x y char color layer bg-color))

(define (posinfo? obj)
  (match obj
    (#('posinfo _x _y _char _color _layer _bg-color) #t)
    (_ #f)))

(define-values (posinfo-x
                posinfo-y posinfo-char
                posinfo-color posinfo-layer
                posinfo-bg-color)
  (apply values
         (map (lambda (i)
                (lambda (pinfo)
                  (vector-ref pinfo (1+ i))))
              (iota 6))))

(define (recolor-posinfo pinfo color)
  #;(_make-posinfo (posinfo-x pinfo)
                 (posinfo-y pinfo)
                 (posinfo-char pinfo)
                 color
                 (posinfo-layer pinfo)
                 (posinfo-bg-color pinfo))
  (match pinfo
    (#('posinfo x y char old-color layer bg-color)
     (vector 'posinfo x y char color layer bg-color))))
  

;; (module+ simpler-posinfo
;;   (provide (rename-out [make-posinfo posinfo])
;;            posinfo?
;;            posinfo-x posinfo-y
;;            posinfo-char posinfo-color
;;            posinfo-layer posinfo-bg-color
;;            recolor-posinfo))

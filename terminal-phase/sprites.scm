(define-module (terminal-phase sprites)
  #:use-module (ice-9 match)
  #:use-module (ice-9 curried-definitions)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib swappable)
  #:use-module (terminal-phase no-op)
  #:use-module (terminal-phase posinfo)
  #:use-module (terminal-phase hollow-swappable)
  #:use-module (terminal-phase level-const)
  #:use-module (terminal-phase loopdown)
  #:use-module (terminal-phase aim)
  #:use-module (terminal-phase add-sub)
  #:use-module (terminal-phase define-slot-actor)
  #:use-module (terminal-phase namespace-env)
  #:export (base-sprite
            ^player-ship
            ^player-bullet
            ^healthy-mixin
            enemy:<:move-speed
            enemy:<:fire-speed
            make-fire-ray
            fire-ray
            fire-collide-ray
            never-fire
            mover-ship-constructor
            ^enemy:<
            ^enemy:<-firing
            ^enemy:<-firing-collides
            ^enemy:squiggler
            ^enemy:slightly-slow-squiggler
            ^enemy:nymph
            ^enemy:kamikaze
            ^screenslider-mixin
            ^enemy:turret
            ^enemy-bullet:ray
            ^enemy-bullet:drifter
            ^enemy-bullet:upbeam
            ^enemy:mine
            ^enemy:up-cannon

            sprites-env))


;;; Sprites and other goblin'y things
;;; =================================

(define base-sprite
  (methods
   ;; Called once per game tick, allows this object to
   ;; "do something interesting" such as move, attack, etc.
   ;; Called with no arguments
   [tick no-op]
   ;; After everyone has taken their actions, collisions are detected.
   ;; If this object has collided with something else, this will be
   ;; called for each relevant collision with the following three arguments:
   ;;  - with: the object we're colliding with
   ;;  - with-posinfo: the particular posinfo structure we bumped into
   ;;  - phase: Which collision "phase" this happened in.  There are really
   ;;    three because due to the weird "live grid action game" nature of Terminal
   ;;    Phase, we need to test against "tentatively, what would happen if I moved
   ;;    based on what the game looked like last time" and also "okay everyone
   ;;    moved did anyone bump into each other?":
   ;;     - 'player-move: the player tries to move, we just test what the
   ;;       effects would be on the world if *just they* moved.  ()The player
   ;;       is currently the only ship that's both tested as they move and
   ;;       after everyone moves... this is to prevent the player "skipping
   ;;       past" enemies)
   ;;     - 'main-tick: any other object that wants to see if a collision would
   ;;       happen if it moves... and checked against the *last* collision
   ;;       positions.  Kinda similar to 'player-move, maybe can be merged
   ;;       eventually.
   ;;     - 'super-collide: the main collision test after *everyone* moves.
   [collide no-op]
   ;; This method takes no arguments and returns a posinfo or list of
   ;; posinfo objects.
   ;; Posinfo objects are both used for display/rendering of characters and for
   ;; collision detection.
   [posinfo (const '())]
   [pre-level-advance no-op]
   ;; This is called every time the level moves forward (ie, the terrain
   ;; moves.)  This is called before the 'tick method.
   [post-level-advance no-op]))

(define (input-free-mixin base-beh)
  (extend-methods base-beh
    ;; Disable input; no moving, no firing.
    [move-down no-op]
    [move-up no-op]
    [move-left no-op]
    [move-right no-op]
    [fire no-op]))

(define (alive-posinfo x y)
  (posinfo ($ x) ($ y)
           #\> 'bryellow
           'player))

(define-actor (^player-ship-core bcom ticky lives x y
                                 moved-this-tick? fire-cooldown)
  #:frozen
  (define (move-deathcheck)
    ($ ticky 'last-collide
       (alive-posinfo x y) 'player-move
       (λ _ ($ ticky 'die)))
    ;; simplifies the "and" which calls this
    #t)

  (extend-methods base-sprite
    [(dead?)
     ($ ticky 'dead?)]
    [(x) ($ x)]
    [(y) ($ y)]
    [(tick)
     ;; Reset whether we moved this tick or not
     ($ moved-this-tick? #f)

     ;; Decrease the fire cooldown, if appropriate
     (unless (= ($ fire-cooldown) 0)
       ($ fire-cooldown (1- ($ fire-cooldown))))]
    ;; returns a boolean (whether we moved or not)
    [(move-up)
     (unless (or ($ moved-this-tick?)
                 (<= ($ y) 0))
       ($ moved-this-tick? #t)
       (cell-sub1 y)
       #t)
     #f]
    [(move-down)
     (unless (or ($ moved-this-tick?)
                 (>= ($ y) (1- LEVEL-HEIGHT)))
       ($ moved-this-tick? #t)
       (cell-add1 y)
       #t)
     #f]
    [(move-left)
     (unless (or ($ moved-this-tick?)
                 (<= ($ x) 0))
       ($ moved-this-tick? #t)
       (cell-sub1 x)
       #t)
     #f]
    [(move-right)
     (unless (or ($ moved-this-tick?)
                 (>= ($ x) (1- LEVEL-WIDTH)))
       ($ moved-this-tick? #t)
       (cell-add1 x)
       #t)
     #f]
    [(fire)
     (when (= ($ fire-cooldown) 0)
       ($ ticky 'to-tick
          (lambda (ticky)
            (spawn ^player-bullet ticky
                   ($ x) ($ y))))
       ($ fire-cooldown 10))]
    [(posinfo) (alive-posinfo x y)]
    [(collide with with-posinfo phase)
     'no-op]
    [move-deathcheck move-deathcheck]
    [(get-lives) ($ lives)]
    [(sub-lives) (cell-sub1 lives)]
    [(die) ($ ticky 'die)]))   ; death by ticky is true death

;;; The four different player states:
;;; alive -> dying -> pause-before-resurrection -> resurrecting

(define-actor (^player-ship::ALIVE bcom metaship-swapper ship-core)
  #:frozen
  (extend-methods ship-core
    ;; Extend all movement actions to do a move-deathcheck
    [(move-up)
     (and ($ ship-core 'move-up)
          ($ ship-core 'move-deathcheck))]
    [(move-down)
     (and ($ ship-core 'move-down)
          ($ ship-core 'move-deathcheck))]
    [(collide with with-posinfo phase)
     (match (posinfo-layer with-posinfo)
       [(or 'enemy 'enemy-bullet 'terrain)
        ;; time to die
        ($ metaship-swapper
           (spawn ^player-ship::DYING metaship-swapper ship-core))]
       [_ 'no-op])]))

(define-slot-actor (^player-ship::DYING bcom metaship-swapper ship-core)
  (^player-ship::DYING*
   (define-cell death-countdown
     (* 30 2))
   (metaship-swapper ship-core death-countdown))

  (extend-methods (input-free-mixin ship-core)
    [(tick)
     ;; one step closer to death...
     (cell-sub1 death-countdown)
     ;; and heck, maybe we're dead
     (when (zero? ($ death-countdown))
       ($ ship-core 'sub-lives)
       (if (zero? ($ ship-core 'get-lives))
           ($ ship-core 'die)
           ($ metaship-swapper
              (spawn ^player-ship::PAUSE-BEFORE-RESURRECTION
                     metaship-swapper ship-core))))]
    [(posinfo)
     (match (modulo (quotient ($ death-countdown) 6) 6)
       [0 (posinfo ($ ship-core 'x) ($ ship-core 'y)
                   #\% 'brred
                   'player)]
       [1 (posinfo ($ ship-core 'x) ($ ship-core 'y)
                   #\# 'bryellow
                   'player)]
       [2 '()]
       [3 (posinfo ($ ship-core 'x) ($ ship-core 'y)
                   #\+ 'red
                   'player)]
       [4 (posinfo ($ ship-core 'x) ($ ship-core 'y)
                   #\# 'yellow
                   'player)]
       [5 '()])]))

(define-slot-actor (^player-ship::PAUSE-BEFORE-RESURRECTION
                    bcom metaship-swapper ship-core)
  (^player-ship::PAUSE-BEFORE-RESURRECTION*
   (define-cell resurrect-countdown
     30)
   (metaship-swapper ship-core resurrect-countdown))
  (extend-methods (input-free-mixin ship-core)
    [(posinfo) '()]
    [(tick)
     (cell-sub1 resurrect-countdown)
     (when (zero? ($ resurrect-countdown))
       ($ metaship-swapper
          (spawn ^player-ship::RESURRECTING
                 metaship-swapper ship-core)))]))

(define-slot-actor (^player-ship::RESURRECTING
                    bcom metaship-swapper ship-core)
  (^player-ship::RESURRECTING*
   (define-cell resurrection-countdown
     (* 30 4))
   (metaship-swapper ship-core resurrection-countdown))
  (extend-methods ship-core
    [(tick)
     ;; call the main tick method first
     ($ ship-core 'tick)
     ;; now reduce time to resurrection
     (cell-sub1 resurrection-countdown)
     (when (zero? ($ resurrection-countdown))
       ($ metaship-swapper
          (spawn ^player-ship::ALIVE
                 metaship-swapper ship-core)))]
    ;; disable handling collisions while resurrecting
    [collide no-op]
    ;; TODO: disable firing while resurrecting?
    #;[(fire) no-op]
    ;; flicker in and out of reality
    [(posinfo)
     (if (even? (quotient ($ resurrection-countdown) 6))
         (posinfo ($ ship-core 'x) ($ ship-core 'y)
                  #\> 'bryellow
                  'player)
         '())]))

(define (^player-ship bcom ticky lives x y)
  (define-cell moved-this-tick? #f)
  (define-cell fire-cooldown 0)
  (define-values (metaship-proxy metaship-swapper)
    (hollow-swappable '^player-ship))
  (define ship-core
    (spawn ^player-ship-core ticky lives x y
           moved-this-tick? fire-cooldown))
  (define initial-alive-metaship
    (spawn ^player-ship::ALIVE metaship-swapper ship-core))
  ($ metaship-swapper initial-alive-metaship)
  metaship-proxy)


(define BULLET-COUNTDOWN 1)

(define-slot-actor (^player-bullet bcom ticky
                                   start-x y)
  (^player-bullet*
   (define-cell x start-x)
   (define-cell countdown BULLET-COUNTDOWN)
   (ticky x y countdown))

  (define (get-posinfos)
    (define cur-x ($ x))
    (define head
      (posinfo (1+ ($ x)) y #\=
               'yellow 'player-bullet))
    (define tail
      (posinfo ($ x) y #\-
               'yellow 'player-bullet))
    (if (< (1+ cur-x) LEVEL-WIDTH)
        (list tail head)
        tail))

  (define (collide with with-posinfo phase)
    (match (posinfo-layer with-posinfo)
      [(or 'enemy 'terrain)
       ;; time to die
       ($ ticky 'die)]
      ['enemy-bullet
       (when ($ with 'kills-player-bullet?)
         ($ ticky 'die))]
      [_ 'no-op]))

  (extend-methods
      base-sprite
    [(tick)
     ;; advance x if appropriate
     (cond
      [(= ($ countdown) 0)
       ($ x (+ ($ x) 2))
       ;; See if we would have died based on our new position
       ;; based on what was previously here; if so, die
       ($ ticky 'last-collide
          (get-posinfos) 'main-tick
          collide)
       ;; Now decrease countdown
       ($ countdown BULLET-COUNTDOWN)]
      [else
       (cell-sub1 countdown)])

     (unless ($ ticky 'dead?)
       (when (>= ($ x) LEVEL-WIDTH)
         ;; we've gone too far
         ($ ticky 'die)))

     'done]
    [(posinfo)
     ;; Write head only if it hasn't gone oob
     (get-posinfos)]
    [collide collide]))


;;; =======
;;; Enemies
;;; =======

;;  - tracks health
;;  - handles PLAYER BULLET collisions only.  Other collision handling
;;    is passed off to the player
;;  - handles the display, usually (unless you override it)
(define-slot-actor (^healthy-mixin _bcom base ticky initial-health
                                   #:key [hit-color 'brwhite])
  (^healthy-mixin*
   ;; TODO: This is a bit wasteful, because there's no need to track
   ;;   health or flashing if the character doesn't have health.
   (define health
     (spawn ^countdown initial-health))

   (define flashing
     (spawn ^countdown 6 0))

   (base ticky health flashing hit-color))

  (define (hit-posinfo pinfo)
    (recolor-posinfo pinfo hit-color))

  (extend-methods base
    ;; remove one from the flashing counter then apply base tick
    ;; method
    [(tick)
     ($ flashing 'sub1)
     ($ base 'tick)]
    [(collide with with-posinfo phase)
     (cond
      ;; Got hit by a player bullet...
      [(eq? (posinfo-layer with-posinfo) 'player-bullet)
       ($ health 'sub1)
       ($ flashing 'reset)
       (when ($ health 'zero?)
         ($ ticky 'die
            #:points? #t))]
      ;; otherwise, let the base handle it
      [else ($ base 'collide with with-posinfo phase)])]
    [(posinfo)
     (define base-posinfos
       ($ base 'posinfo))
     (if ($ flashing 'zero?)
         ;; not flashing, show it as-is
         base-posinfos
         ;; otherwise, we need to invert all of them
         (if (pair? base-posinfos)
             (map hit-posinfo base-posinfos)
             (hit-posinfo base-posinfos)))]))


;;; Ships
;;; -----

(define enemy:<:move-speed
  4)
(define enemy:<:fire-speed
  (* enemy:<:move-speed 20))

(define (move-forward ticky x y)
  (cell-sub1 x))

(define* ((make-fire-ray #:key [collide? #f])
          ticky bullet-x bullet-y)
  ($ ticky 'to-tick
     (lambda (ticky)
       (spawn ^enemy-bullet:ray ticky bullet-x bullet-y
              #:collide? collide?))))

(define fire-ray (make-fire-ray))
(define fire-collide-ray (make-fire-ray #:collide? #t))

(define (never-fire . args)
  'no-op)

(define* (mover-ship-constructor char color
                                 #:key [move-speed enemy:<:move-speed]
                                 [fire-speed enemy:<:fire-speed]
                                 [move move-forward]
                                 [fire fire-ray])
  (define-slot-actor (^constructa-ship bcom ticky initial-x initial-y
                                       #:key
                                       mover firer
                                       [move-speed move-speed])
    (^constructa-ship*
     (define-cell x initial-x)
     (define-cell y initial-y)
     (define move-countdown
       (spawn ^loopdown move-speed))
     (define fire-countdown
       (spawn ^loopdown fire-speed (quotient fire-speed 3)))
     (ticky x y move-countdown fire-countdown mover firer))

    (extend-methods
     base-sprite
     [(tick)
      (when ($ move-countdown 'zero?)
        (if mover
            ($ mover ticky x y)
            (move ticky x y)))

      (cond
        ;; Left the screen?  time to die
        [(oob? ($ x) ($ y))
         ($ ticky 'die)]

        ;; Still on screen?  Guess we're still alive
        [else
         ;; Fire a bullet if it's bullet firing time
         (when ($ fire-countdown 'zero?)
           (let ([bullet-x (1- ($ x))]
                 [bullet-y ($ y)])
             (unless (oob? bullet-x bullet-y)
               (if firer
                   ($ firer ticky bullet-x bullet-y)
                   (fire ticky bullet-x bullet-y)))))

         ($ move-countdown 'sub1)
         ($ fire-countdown 'sub1)
         'done])]
     [(posinfo)
      (posinfo ($ x) ($ y) char color
               'enemy)]
     [(collide with with-posinfo phase)
      (match (posinfo-layer with-posinfo)
        [(or 'player-bullet)
         ;; time to die
         ($ ticky 'die #:points? #t)]
        [_ 'no-op])]))
  (values ^constructa-ship ^constructa-ship*))

(define-values (^enemy:< ^enemy:<*)
  (mover-ship-constructor #\< 'yellow
                          #:fire no-op))

(define-values (^enemy:<-firing ^enemy:<-firing*)
  (mover-ship-constructor #\< 'cyan))

(define-values (^enemy:<-firing-collides ^enemy:<-firing-collides*)
  (mover-ship-constructor #\< 'green
                          #:fire fire-collide-ray))

(define squiggler-pattern
  #(-1 -1 0 0 0 +1 +1 +1 0 0 0 -1))


(define-actor (^enemy:squiggler::MOVER bcom squiggle-pos)
  (lambda (ticky x y)
    ;; first move to the left
    (cell-sub1 x)
    ;; then squiggle
    (define squiggle-offset
      (vector-ref squiggler-pattern
                  ($ squiggle-pos 'counter)))
    ($ y (+ ($ y) squiggle-offset))  ; set y to y plus squiggle-offset
    ;; push down squiggle counter
    ($ squiggle-pos 'sub1)))

(define-values (^enemy:squiggler::CORE ^enemy:squiggler::CORE*)
  (mover-ship-constructor #\S 'blue
                          #:move-speed 4
                          #:fire never-fire))

(define-values (^enemy:slower-squiggler::CORE ^enemy:slower-squiggler::CORE*)
  (mover-ship-constructor #\S 'brblue
                          #:move-speed 5
                          #:fire never-fire))

(define (spawn-squiggle-pos)
  (spawn ^loopdown (1- (vector-length squiggler-pattern))))

(define* (^enemy:squiggler bcom ticky initial-x initial-y)
  (define squiggle-pos (spawn-squiggle-pos))
  (define shuffer (spawn ^enemy:squiggler::MOVER squiggle-pos))
  (spawn ^enemy:squiggler::CORE ticky initial-x initial-y
         #:mover shuffer))

(define* (^enemy:slightly-slow-squiggler bcom ticky initial-x initial-y)
  (define squiggle-pos (spawn-squiggle-pos))
  (define shuffer (spawn ^enemy:squiggler::MOVER squiggle-pos))
  (spawn ^enemy:slower-squiggler::CORE ticky initial-x initial-y
         #:mover shuffer))

(define-values (^enemy:nymph::CORE ^enemy:nymph::CORE*)
  (mover-ship-constructor #\n 'magenta
                          #:fire never-fire))

;; Move behavior; shuffs closer and closer to player
(define-actor (^enemy:nymph::SHUFFER bcom shuf-countdown)
  (lambda (ticky x y)
    ;; first, move forward
    (move-forward ticky x y)
    (when ($ shuf-countdown 'zero?)
      ;; now determine if we need to "slide" closer to the player
      (match ($ ticky 'player-pos)
        ((_player-x . player-y)
         (cond
          [(< ($ y) player-y)
           (cell-add1 y)]
          [(> ($ y) player-y)
           (cell-sub1 y)]))))
    ($ shuf-countdown 'sub1)))

(define* (^enemy:nymph bcom ticky initial-x initial-y
                       #:key [shuf-countdown-every 5]
                       [move-speed 3])
  (define shuf-countdown
    (spawn ^loopdown shuf-countdown-every))
  (define shuffer
    (spawn ^enemy:nymph::SHUFFER shuf-countdown))

  (spawn ^enemy:nymph::CORE ticky initial-x initial-y
         #:mover shuffer
         #:move-speed move-speed))

;; Kamikaze moves in, tries to align itself with the player,
;; locks on, and then RAPIDLY hurtles towards the player
(define-actor (^enemy:kamikaze::CORE bcom ticky x y)
  #:frozen
  (extend-methods base-sprite
    [(posinfo)
     (posinfo ($ x) ($ y) #\K 'brmagenta
              'enemy)]
    [(collide with with-posinfo phase)
     (match (posinfo-layer with-posinfo)
       [(or 'player 'terrain)
        ;; time to die
        ($ ticky 'die)]
       [_ 'no-op])]))

(define-slot-actor (^enemy:kamikaze::PLOD-IN _bcom ticky x y
                                             meta-swapper core)
  (^enemy:kamikaze::PLOD-IN*
   (define move-countdown
     (spawn ^loopdown 5))
   (define plods-left
     (spawn ^countdown 2))
   (ticky x y meta-swapper core move-countdown plods-left))
  (extend-methods core
    [(tick)
     (when ($ move-countdown 'zero?)
       (move-forward ticky x y)
       ($ plods-left 'sub1))
     ($ move-countdown 'sub1)
     (when ($ plods-left 'zero?)
       ($ meta-swapper
          (spawn ^enemy:kamikaze::AIM-SELF
                 ticky x y meta-swapper core)))]))

(define-slot-actor (^enemy:kamikaze::AIM-SELF _bcom ticky x y
                                              meta-swapper core)
  (^enemy:kamikaze::AIM-SELF*
   (define shuf-countdown
     (spawn ^loopdown 12))
   (define shufs-left
     (spawn ^countdown 8))
   (ticky x y meta-swapper core shuf-countdown shufs-left))

  (extend-methods core
    [(tick)
     (when ($ shuf-countdown 'zero?)
       (match ($ ticky 'player-pos)
         ((_player-x . player-y)
          (cond
           [(< ($ y) player-y)
            (cell-add1 y)]
           [(> ($ y) player-y)
            (cell-sub1 y)])))
       ($ shufs-left 'sub1))

     ($ shuf-countdown 'sub1)

     (when ($ shufs-left 'zero?)
       ($ meta-swapper
          (spawn ^enemy:kamikaze::PAUSE-AND-FLASH
                 ticky x y meta-swapper core)))]))

(define-slot-actor (^enemy:kamikaze::PAUSE-AND-FLASH _bcom ticky x y
                                                     meta-swapper core)
  (^enemy:kamikaze::PAUSE-AND-FLASH*
   (define flash-countdown
     (spawn ^countdown 15))
   (ticky x y meta-swapper core flash-countdown))

  (extend-methods core
    [(tick)
     ($ flash-countdown 'sub1)
     (when ($ flash-countdown 'zero?)
       ($ meta-swapper
          (spawn ^enemy:kamikaze::RACE-TO-DEATH
                 ticky x y meta-swapper core)))]
    [(posinfo)
     (posinfo ($ x) ($ y) #\K
              (if (even? (quotient ($ flash-countdown 'counter) 5))
                  'red
                  'brmagenta)
              'enemy)]))

(define-actor (^enemy:kamikaze::RACE-TO-DEATH _bcom ticky x y
                                              meta-swapper core)
  #:frozen
  (extend-methods core
    [(tick)
     (move-forward ticky x y)
     (when (oob? ($ x) ($ y))
       ($ ticky 'die))]))

(define (^enemy:kamikaze bcom ticky initial-x initial-y)
  (define-cell x initial-x)
  (define-cell y initial-y)

  (define kamikaze-core
    (spawn ^enemy:kamikaze::CORE ticky x y))
  (define-values (meta-proxy meta-swapper)
    (hollow-swappable '^enemy:kamikaze::META))
  ($ meta-swapper
     (spawn ^enemy:kamikaze::PLOD-IN ticky x y
            meta-swapper kamikaze-core))

  (define healthy-kamikaze
    (spawn-named '^enemy:kamikaze
                 ^healthy-mixin meta-proxy ticky 3))

  healthy-kamikaze)

(define-slot-actor (^screenslider-mixin bcom x y ticky
                                        #:key base)
  (^screenslider-mixin*
   (define-cell have-i-run?
     #f)
   (x y ticky base have-i-run?))
  (extend-methods (or base base-sprite)
   [(pre-level-advance)
    ($ have-i-run? #t)]
   [(post-level-advance)
    (when ($ have-i-run?)
      (cell-sub1 x))
    ;; die if we pass off screen
    (when (oob? ($ x) ($ y))
      ($ ticky 'die))]))

(define turret-fire-speed
  80)

(define-actor (^enemy:turret::CORE bcom ticky x y fire-cooldown)
  #:frozen
  (define screenslider
    (spawn ^screenslider-mixin x y ticky))
  (extend-methods screenslider
    [(tick)
     (when ($ fire-cooldown 'zero?)
       ($ ticky 'to-tick
          (λ (ticky)
            (match ($ ticky 'player-pos)
              ((player-x . player-y)
               (unless (and (eqv? ($ x) player-x)
                            (eqv? ($ y) player-y))
                 (spawn ^enemy-bullet:drifter ticky
                        ($ x) ($ y)
                        player-x player-y
                        ;; Penalize bullets that are moving to the right
                        ;; to handle visual weirdness
                        #:speed (if (>= (- player-x ($ x)) 0)
                                    0.115 .2))))))))
     ($ fire-cooldown 'sub1)]

    [(posinfo)
     (posinfo ($ x) ($ y)
              #\I 'brwhite
              'enemy)]

    ;; we also die on player impact
    [(collide with with-posinfo phase)
     (match (posinfo-layer with-posinfo)
       ['player
        ($ ticky 'die)]
       [_ 'no-op])]))

(define (^enemy:turret bcom ticky initial-x initial-y)
  (define-cell x initial-x)
  (define-cell y initial-y)

  (define fire-cooldown
    (spawn ^loopdown turret-fire-speed 30))

  (define turret-core
    (spawn ^enemy:turret::CORE ticky x y fire-cooldown))

  (define healthy-turret
    (spawn-named '^enemy:turret
                 ^healthy-mixin turret-core
                 ticky 3
                 #:hit-color 'brred))

  healthy-turret)

;;; Bullets
;;; -------

(define enemy-bullet:move-speed
  2)

(define-slot-actor (^enemy-bullet:ray bcom ticky x y
                                      #:key [collide? #t])
  (^enemy-bullet:ray*
   (define move-countdown
     (spawn-named 'move-countdown
                  ^loopdown enemy-bullet:move-speed))
   (ticky x y collide? move-countdown))
  
  (extend-methods base-sprite
    [(tick)
     ($ move-countdown 'sub1)
     (define next-x
       (if ($ move-countdown 'zero?)
           (1- x)
           x))
     (cond
      [(oob? next-x y)
       ($ ticky 'die)]
      [else
       (bcom (^enemy-bullet:ray* bcom ticky next-x
                                 y collide?
                                 move-countdown))])]
    [(posinfo)
     (posinfo x y #\-
              (if collide?
                  'brcyan
                  'white)
              'enemy-bullet)]
    [(collide with with-posinfo phase)
     (match (posinfo-layer with-posinfo)
       [(or 'player 'terrain)
        ;; time to die
        ($ ticky 'die)]
       ['player-bullet
        (when collide?
          ($ ticky 'die))]
       [_ 'no-op])]
    [(kills-player-bullet?)
     collide?]))

(define-slot-actor (^enemy-bullet:drifter bcom ticky
                                          start-x start-y
                                          target-x target-y
                                          #:key [speed 0.2]
                                          [lifetime #f]
                                          [char #\*]
                                          [color 'brwhite]
                                          [shootable? #f]
                                          [crashable? #t])
  (^enemy-bullet:drifter*
   (when (and (eqv? start-x target-x)
              (eqv? start-y target-y))
     (error "Can't fire at self"))

   (define aimer
     (spawn ^aimer
            start-x start-y
            target-x target-y
            #:speed speed))
   (define lifetime-tracker
     (and lifetime
          (spawn ^countdown lifetime)))

   (ticky aimer lifetime-tracker char color shootable? crashable? lifetime))

  (extend-methods base-sprite
    [(tick)
     ($ aimer 'move)

     ;; decrement lifetime if appropriate
     (when lifetime
       (when ($ lifetime-tracker 'zero?)
         ($ ticky 'die))
       ($ lifetime-tracker 'sub1))

     (match ($ aimer 'pos)
       (#(x y)
        (when ($ ticky 'oob? x y)
          ($ ticky 'die))))]
    [(posinfo)
     (match ($ aimer 'pos)
       (#(x y)
        (posinfo x y char color
                 'enemy-bullet)))]
    [(collide with with-posinfo phase)
     (match (posinfo-layer with-posinfo)
       ['player
        ;; time to die
        ($ ticky 'die)]
       ['player-bullet
        (when shootable?
          ($ ticky 'die))]
       ['terrain
        (when crashable?
          ($ ticky 'die))]
       [_ 'no-op])]
    [(kills-player-bullet?)
     shootable?]))

(define-slot-actor (^enemy-bullet:upbeam bcom ticky initial-x initial-y)
  (^enemy-bullet:upbeam*
   (define x
     (spawn ^cell initial-x))
   (define y
     (spawn ^cell initial-y))
   (define screenslider
     (spawn ^screenslider-mixin x y ticky))
   (ticky x y screenslider))
  (extend-methods screenslider
   [(tick)
    (define next-y
      (1- ($ y)))
    (if (< next-y 0)
        ;; we went out of bounds
        ($ ticky 'die)
        ;; otherwise move up
        ($ y next-y))]
   [(posinfo)
    (posinfo ($ x) ($ y) #\|
             'bryellow
             'enemy-bullet)]
   [(collide with with-posinfo phase)
    (match (posinfo-layer with-posinfo)
      ['terrain
       ;; time to die
       ($ ticky 'die)]
      [_ 'no-op])]
   [(kills-player-bullet?)
    #f]))

(define-actor (^enemy:mine::CORE bcom ticky x y)
  #:frozen
  (define screenslider
    (spawn ^screenslider-mixin x y ticky))

  (extend-methods screenslider
    [(posinfo)
     (posinfo ($ x) ($ y)
              #\+ 'brgreen
              'enemy)]
    ;; Additionally, we die if a player collides with us
    [(collide with with-posinfo phase)
     (match (posinfo-layer with-posinfo)
       ['player
        ;; time to die
        ($ ticky 'die #:points? #t)]
       [_ 'no-op])]))

(define (^enemy:mine bcom ticky initial-x initial-y)
  (define x
    (spawn ^cell initial-x))
  (define y
    (spawn ^cell initial-y))
  (define mine-core
    (spawn ^enemy:mine::CORE ticky x y))
  (define healthy-mine
    (spawn-named '^enemy:mine
                 ^healthy-mixin
                 mine-core ticky 2))
  healthy-mine)

(define-actor (^enemy:up-cannon::IDLE bcom ticky x y
                                      meta-swapper screenslider
                                      #:optional [countdown 30])
  #:frozen
  (extend-methods screenslider
    [(posinfo)
     (posinfo ($ x) ($ y)
              #\^ 'brcyan
              'enemy)]
    [(tick)
     (if (zero? countdown)
         ($ meta-swapper
            (spawn ^enemy:up-cannon::FIRING
                   ticky x y meta-swapper screenslider))
         (bcom (^enemy:up-cannon::IDLE bcom ticky x y meta-swapper
                                       screenslider (1- countdown))))]))

(define-actor (^enemy:up-cannon::FIRING bcom ticky x y
                                        meta-swapper screenslider
                                        #:optional [countdown 10])
  #:frozen
  (extend-methods screenslider
    [(posinfo)
     (posinfo ($ x) ($ y)
              #\^ 'bryellow
              'enemy)]
    [(tick)
     (if (zero? countdown)
         ($ meta-swapper
            (spawn ^enemy:up-cannon::IDLE
                   ticky x y meta-swapper screenslider))
         (begin
           ($ ticky 'to-tick
              (lambda (ticky)
                (spawn ^enemy-bullet:upbeam ticky
                       ($ x) ($ y))))
           (bcom (^enemy:up-cannon::FIRING bcom ticky x y
                                           meta-swapper screenslider
                                           (1- countdown)))))]))

(define (^enemy:up-cannon bcom ticky initial-x initial-y)
  (define x
    (spawn ^cell initial-x))
  (define y
    (spawn ^cell initial-y))
  (define screenslider
    (spawn ^screenslider-mixin x y ticky))

  ;; This proxies the "main behavior" of the cannon
  (define-values (meta-proxy meta-swapper)
    (hollow-swappable '^enemy:up-cannon::META))

  ;; Initialize the idle state
  (define idle-cannon
    (spawn ^enemy:up-cannon::IDLE ticky x y
           meta-swapper screenslider))
  ($ meta-swapper idle-cannon)

  (define healthy-cannon
    (spawn-named '^enemy:up-cannon
                 ^healthy-mixin meta-proxy ticky 3))
  healthy-cannon)

(define-namespace-env sprites-env
  (terminal-phase sprites)
  ^player-ship-core
  ^player-ship::ALIVE
  (^player-ship::DYING ^player-ship::DYING*)
  (^player-ship::PAUSE-BEFORE-RESURRECTION ^player-ship::PAUSE-BEFORE-RESURRECTION*)
  (^player-ship::RESURRECTING ^player-ship::RESURRECTING*)
  (^player-bullet ^player-bullet*)
  (^healthy-mixin ^healthy-mixin*)
  (^enemy:< ^enemy:<*)
  (^enemy:<-firing ^enemy:<-firing*)
  (^enemy:<-firing-collides ^enemy:<-firing-collides*)
  ^enemy:squiggler::MOVER
  (^enemy:squiggler::CORE ^enemy:squiggler::CORE*)
  (^enemy:nymph::CORE ^enemy:nymph::CORE*)
  ^enemy:nymph::SHUFFER
  ^enemy:kamikaze::CORE
  (^enemy:kamikaze::PLOD-IN ^enemy:kamikaze::PLOD-IN*)
  (^enemy:kamikaze::AIM-SELF ^enemy:kamikaze::AIM-SELF*)
  (^enemy:kamikaze::PAUSE-AND-FLASH ^enemy:kamikaze::PAUSE-AND-FLASH*)
  ^enemy:kamikaze::RACE-TO-DEATH
  (^screenslider-mixin ^screenslider-mixin*)
  ^enemy:turret::CORE
  (^enemy-bullet:ray ^enemy-bullet:ray*)
  (^enemy-bullet:drifter ^enemy-bullet:drifter*)
  (^enemy-bullet:upbeam ^enemy-bullet:upbeam*)
  ^enemy:mine::CORE
  ^enemy:up-cannon::IDLE
  ^enemy:up-cannon::FIRING
  #:extends (list cell-env aim-env loopdown-env swappable-env))

(define-module (terminal-phase ring-buffer)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:export (make-ring-buffer
            ring-buffer-insert!
            ring-buffer-newest
            ring-buffer-oldest
            ring-buffer->fifo-list
            ring-buffer->lifo-list))

(define-record-type <ring-buffer>
  (make-ring-buffer vec pos)
  ring-buffer?
  (vec ring-buffer-vec)
  (pos ring-buffer-pos set-ring-buffer-pos!))

(define* (make-ring-buffer size #:optional [initial-val #f])
  (make-ring-buffer (make-vector size initial-val)
                    0))

(define (ring-buffer-insert! rbuf val)
  (define vec (ring-buffer-vec rbuf))
  (define pos (ring-buffer-pos rbuf))
  (vector-set! (ring-buffer-vec rbuf) pos val)
  (set-ring-buffer-pos! rbuf (modulo (1+ pos) (vector-length vec))))

;; (module+ test
;;   (require rackunit)
;;   (define rb (make-ring-buffer 5))
;;   (ring-buffer-insert! rb 'a)
;;   (ring-buffer-insert! rb 'b)
;;   (check-equal?
;;    (ring-buffer-vec rb)
;;    '#(a b #f #f #f))
;;   (ring-buffer-insert! rb 'c)
;;   (ring-buffer-insert! rb 'd)
;;   (ring-buffer-insert! rb 'e)
;;   (ring-buffer-insert! rb 'f)
;;   (check-equal?
;;    (ring-buffer-vec rb)
;;    '#(f b c d e)))

(define (ring-buffer-newest rbuf)
  (define vec (ring-buffer-vec rbuf))
  (define pos (ring-buffer-pos rbuf))
  (vector-ref vec (modulo (1- pos) (vector-length vec))))

(define (ring-buffer-oldest rbuf)
  (define vec (ring-buffer-vec rbuf))
  (define pos (ring-buffer-pos rbuf))
  (vector-ref vec pos))

(define (ring-buffer->lifo-list rbuf)
  (define vec (ring-buffer-vec rbuf))
  (define pos (ring-buffer-pos rbuf))
  (define vec-len (vector-length vec))
  (let lp ([offset 0])
    (if (= offset vec-len)
        '()
        (cons (vector-ref vec (modulo (- pos offset 1) vec-len))
              (lp (1+ offset))))))

(define (ring-buffer->fifo-list rbuf)
  (define vec (ring-buffer-vec rbuf))
  (define pos (ring-buffer-pos rbuf))
  (define vec-len (vector-length vec))
  (let lp ([offset vec-len])
    (if (zero? offset)
        '()
        (cons (vector-ref vec (modulo (- pos offset) vec-len))
              (lp (1- offset))))))

;; (module+ test
;;   (check-equal?
;;    (ring-buffer-newest rb)
;;    'f)
;;   (check-equal?
;;    (ring-buffer-oldest rb)
;;    'b)
;;   (check-equal?
;;    (ring-buffer->lifo-list rb)
;;    '(f e d c b))
;;   (check-equal?
;;    (ring-buffer->fifo-list rb)
;;    '(b c d e f)))

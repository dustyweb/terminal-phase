(define-module (terminal-phase randrange)
  #:export (randrange))

(define (randrange min max)
  (+ min (random (- max min))))

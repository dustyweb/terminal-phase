(define-module (terminal-phase aim)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib cell)
  #:use-module (terminal-phase define-slot-actor)
  #:use-module (terminal-phase namespace-env)
  #:export (^aimer aim-env))


;; We multiply the Y space by two and then scale it back down before
;; applying because otherwise the speed is goofy since characters are
;; so much taller than they are wide

(define-slot-actor (^aimer bcom
                           start-x start-y
                           target-x target-y
                           #:key [speed .3])
  (^aimer*
   (define-cell traveled 0)
   (start-x start-y target-x target-y speed traveled))
  (define dx
    (- target-x start-x))
  (define dy
    (* (- target-y start-y) 2.0))
  (define distance  ; or magnitude
    (sqrt (+ (* dx dx) (* dy dy))))
  (define normalized-dx
    (if (zero? distance)
        0.0
        (/ dx distance)))
  (define normalized-dy
    (if (zero? distance)
        0
        (/ dy distance)))
  (define (inexact-pos)
    (define cur-x
      (+ start-x (* ($ traveled) normalized-dx)))
    (define cur-y
      (+ start-y (/ (* ($ traveled) normalized-dy) 2.0)))
    (values cur-x cur-y))

  (methods
   [(move #:optional [speed speed])
    ($ traveled (+ ($ traveled) speed))]
   [(pos)
    (define-values (x y)
      (inexact-pos))
    (vector (inexact->exact (floor x))
            (inexact->exact (floor y)))]
   [(precise-pos)
    (define-values (x y)
      (inexact-pos))
    (vector x y)]
   [(traveled)
    ($ traveled)]))

(define aim-env
  (namespace-env (terminal-phase aim)
                 (^aimer ^aimer*)
                 #:extends cell-env))

#;(module+ test
  (require rackunit)
  (define am (make-actormap))

  (define aimer1
    (actormap-spawn! am ^aimer
                     1 2
                     4 8))

  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(1 2))
  (actormap-poke! am aimer1 'move)
  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(1 2))
  (actormap-poke! am aimer1 'move)
  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(1 2))
  (actormap-poke! am aimer1 'move)
  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(1 2))
  (actormap-poke! am aimer1 'move)
  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(1 3))
  (actormap-poke! am aimer1 'move)
  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(1 3))
  (actormap-poke! am aimer1 'move)
  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(1 3))
  (actormap-poke! am aimer1 'move)
  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(1 3))
  (actormap-poke! am aimer1 'move)
  (check-equal?
   (actormap-peek am aimer1 'pos)
   #(2 4)))

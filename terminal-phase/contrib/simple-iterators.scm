;;; Copyright 2022 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;; originally from https://gitlab.com/dustyweb/guile-simple-iterators

(define-module (terminal-phase contrib simple-iterators)
  #:use-module (srfi srfi-1)
  #:export (folder
            for/folder
            for for/map for/c
            for/fold for/fold-right))

; Emacs: (put 'for 'scheme-indent-function 1)
(define-syntax-rule (for (i lst)
                       body ...)
  (for-each (lambda (i)
              body ...)
            lst))

; Emacs: (put 'for/map 'scheme-indent-function 1)
(define-syntax-rule (for/map (x lst)
                      body ...)
  (map (lambda (x) body ...) lst))

; Emacs: (put 'for/c 'scheme-indent-function 1)

(define-syntax-rule (for/c ((i initial) update stop?)
                       body ...)
  (let for-loop ((i initial))
    (unless stop?
      ;; wrapped in lambda to allow defines
      ((lambda ()
         body ...
         (for-loop update))))))

; Emacs: (put 'for/fold 'scheme-indent-function 1)
(define-syntax-rule (for/fold ((result seed) (item lst) ...)
                       body ...)
  (fold (lambda (item ... result)
          body ...)
        seed
        lst ...))

; Emacs: (put 'for/fold-right 'scheme-indent-function 1)
(define-syntax-rule (for/fold-right ((result seed) (item lst) ...)
                       body ...)
  (fold-right (lambda (item ... result)
                body ...)
              seed
              lst ...))

(define (folder proc val seq next stop?)
  (if (stop? seq)
      val
      (folder proc (proc seq val) (next seq) next stop?)))

; Emacs: (put 'for/folder 'scheme-indent-function 1)

(define-syntax-rule (for/folder ((iter init-iter) update stop?
                                 (val seed-val))
                      body ...)
  (let folder-lp ((iter init-iter)
                  (val seed-val))
    (if stop?
        val
        (folder-lp update
                   ;; wrapped in lambda to allow defines
                   ((lambda () body ...))))))

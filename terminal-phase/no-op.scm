(define-module (terminal-phase no-op)
  #:export (no-op))

(define no-op (lambda _ 'no-op))

(define-module (terminal-phase game-loop)
  #:use-module (ice-9 match)
  #:use-module (ncurses curses)
  #:use-module (terminal-phase ncurses-stuff)
  #:use-module (system repl coop-server)
  #:export (screen-setup!
            run-game-loop))

(define (get-usecs-now)
  (match (gettimeofday)
    ((secs . usec-remainder)
     (+ (* secs 1000000)
        usec-remainder))))

(define (consume-all-input handle-input screen halt!)
  (define (loop)
    (match (getch screen)
      (#f 'done)
      (input
       (handle-input screen input halt!)
       (loop))))
  (loop))

(define* (screen-setup! #:optional (screen (initscr)))
  (noecho!)                ; disable echoing characters
  (raw!)                   ; don't buffer input
  (keypad! screen #t)      ; enable <f1>, arrow keys, etc
  (start-color!)           ; turn on colors
  (curs-set 0)             ; hide cursor
  (nodelay! screen #t)
  (install-colors!)        ; enable specific colors
  screen)

(define* (run-game-loop do-update
                        handle-input
                        #:key
                        (screen (screen-setup!))
                        (repl #f)
                        (tick-usecs (truncate-quotient 1000000 60)))
  (define running? #t)
  (define (halt!)
    (set! running? #f))
  (define (game-loop)
    (define last-usecs (get-usecs-now))
    (consume-all-input handle-input screen halt!)
    (when running?
      (do-update screen halt!))
    (when repl
      (poll-coop-repl-server repl))
    (when running?
      (let* ((before-sleep (get-usecs-now))
             (delay-usecs (max (- (+ last-usecs tick-usecs)
                                  before-sleep)
                               0)))
        (usleep delay-usecs))
      (game-loop)))
  (define (setup) 'no-op)
  (define (handle-error exn)
    (endwin)
    (format (current-error-port) "Got exception: ~a\n\n" exn)
    (display-backtrace (make-stack #t)
                       (current-error-port))
    (newline (current-error-port)))
  (with-exception-handler handle-error
    (lambda ()
      (game-loop)
      (endwin))
    #:unwind? #f
    #:unwind-for-type #t))

(define-module (terminal-phase credits)
  #:use-module (terminal-phase globals)
  #:use-module (terminal-phase level-const)
  #:use-module (terminal-phase loopdown)
  #:use-module (terminal-phase ncurses-stuff)
  #:use-module (terminal-phase no-op)
  #:use-module (terminal-phase paths)
  #:use-module (terminal-phase randrange)
  #:use-module (ice-9 match)
  #:use-module ((ice-9 textual-ports)
                #:select (get-string-all))
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib cell)
  #:use-module ((ncurses curses)
                #:prefix curses:)
  #:export (^credits
            ^text-buffer))

(define (get-credits-filename)
  (string-append (get-extra-dir) file-name-separator-string "credits-roll.txt"))

(define (credits-lines)
  (define credits-text
    (call-with-input-file (get-credits-filename) get-string-all))
  (string-split credits-text #\newline))

(define (^text-buffer bcom num-lines width)
  (define* (next #:optional
                 [wip-line '()]
                 ;; We store all but one in here...
                 ;; that last one is in the wip-line
                 [stored-lines
                  (make-vector (1- num-lines)
                               '())])  ; lines are lists, so empty line

    (methods
     [(insert-char char color #:key bold?)
      (bcom (next (cons (if bold?
                            (curses:bold (curses:color color char))
                            (curses:color color char))
                        wip-line)
                  stored-lines))]
     [(data)
      (vector wip-line stored-lines)]
     [(crlf)
      (define new-stored-lines
        (make-vector (1- num-lines)
                     (reverse wip-line)))
      (vector-copy! new-stored-lines 1
                    stored-lines
                    0
                    (1- (vector-length stored-lines)))
      (bcom (next '() new-stored-lines))]
     [(render screen #:optional [with-cursor? #f])
      (define (render-line line y)
        (do ((chars line (cdr chars))
             (x 0 (1+ x)))
            ((null? chars))
          (curses:addch screen (car chars)
                        #:y y #:x x)))
      ;; write out stored lines
      (let ((num-stored-lines (vector-length stored-lines)))
        (do ((i 0 (1+ i)))
            ((= i num-stored-lines))
          (let ((y (- num-lines i 1)))
            (render-line (vector-ref stored-lines i) y))))

      ;; write out wip line
      (let ((y num-lines))  ; goes on the last line
        (render-line (reverse wip-line) y)
        ;; maybe write out the cursor
        (when with-cursor?
          ;; Well... this SHOULD work, but guess what, it doesn't.
          ;; There's some kinda bug in guile-ncurses where outputting
          ;; xchars for non-ascii characters just renders a "?" character.
          ;; We ought to file a bug if one isn't filed yet...
          #;(curses:addch screen (curses:normal #\█)
                        #:y y
                        #:x (length wip-line))
          (curses:attr-set! screen curses:A_BOLD %GREEN-N)
          (curses:addstr screen "█"
                         #:y y
                         #:x (length wip-line))
          ;; Ok now unset it.
          (curses:attr-set! screen curses:A_NORMAL)))
      ;; (define the-text
      ;;   (raart:vappend
      ;;    #:halign 'left
      ;;    (raart:vappend*
      ;;     #:halign 'left
      ;;     (reverse (vector->list stored-lines)))
      ;;    (wip-line->raart)))
      ;; (define matted
      ;;   (raart:matte width num-lines
      ;;                #:halign 'left
      ;;                the-text))
      ;; (define with-cursor
      ;;   (if with-cursor?
      ;;       (raart:place-at matted
      ;;                       (1- num-lines) (length wip-line)
      ;;                       (raart:fg 'brgreen (raart:char #\█)))  ; or ▒?
      ;;       matted))
      ;; with-cursor
      ]))
  (next))


(define (^credits bcom)
  (define text-buffer
    (spawn ^text-buffer 20 50))

  (define base-methods
    (methods
     [tick no-op]
     [(screen-size)
      (cons (+ LEVEL-WIDTH 4)
	    (+ LEVEL-HEIGHT 8))]
     [(handle-event evt)
      (match evt
        [(or #\q #\space #\return #\newline)
	 (set! %resize-screen? #t)
         ($ (%dpr-pushdown) 'pop)]
        [_
         'no-op])]
     [(render screen)
      ($ text-buffer 'render screen #t)]
     [(get-text-buffer) text-buffer]))

  (define hang-until-quit base-methods)

  #;(define (boot)
    'TODO)

  (define (make-blinker-mixin)
    (define-cell on? #t)
    (define toggle-countdown
      (spawn ^loopdown 15))
    (define (mixin base)
      (extend-methods base
        [(tick)
        ;; time to toggle
        (when ($ toggle-countdown 'zero?)
          ($ on? (not ($ on?))))

        ($ toggle-countdown 'sub1)

        (base 'tick)]
       [(render screen)
        ($ text-buffer 'render screen ($ on?))]))
    mixin)

  (define (next-line-handler lines)
    (match lines
      [() hang-until-quit]
      [(next-line . rest-lines)
       (let ((line-len (string-length next-line)))
         (cond
          [(and (>= line-len 2)
                (equal? (substring next-line 0 2) "> "))
           (read-prompt-line next-line rest-lines)]
          [(and (>= line-len 1)
                (char=? (string-ref next-line 0) #\%))
           (brief-pause rest-lines)]
          [else
           (read-normal-line next-line rest-lines)]))]))

  (define (read-prompt-line line rest-lines)
    (define blinker-mixin
      (make-blinker-mixin))
    (define line-chars
      (string->list (substring line 2)))
    ;; insert the prompt and a space
    ($ text-buffer 'insert-char #\> %YELLOW-N #:bold? #t)
    ($ text-buffer 'insert-char #\space %WHITE-N #:bold? #t)

    (define (pause-before-typing)
      (define countdown
        (spawn ^countdown (* 30 3)))
      (blinker-mixin
       (extend-methods base-methods
        [(tick)
         ($ countdown 'sub1)
         (when ($ countdown 'zero?)
           (bcom (type-it)))])))

    (define (type-it)
      (define* (next #:optional [chars line-chars])
        (define type-pause
          ;; some variance in how fast we type for realism
          (spawn ^countdown (randrange 2 10)))

        (blinker-mixin
         (extend-methods base-methods
          [(tick)
           ($ type-pause 'sub1)
           (when ($ type-pause 'zero?)
             (match chars
               ;; we're done, move to next line
               [()
                ($ text-buffer 'crlf)
                (bcom (next-line-handler rest-lines))]
               ;; otherwise add this character
               [(this-char . rest-chars)
                ($ text-buffer 'insert-char this-char %WHITE-N)
                (bcom (next rest-chars))]))])))
      (next))

    (pause-before-typing))

  ;; code duplication but I am tired
  (define (read-normal-line line rest-lines)
    (define line-chars
      (string->list line))

    #;(define type-pause
      (spawn ^loopdown 0))

    (define* (next #:optional [chars line-chars])
      (extend-methods base-methods
       [(tick)
        ;; ($ type-pause 'sub1)
        ;; (when ($ type-pause 'zero?))
        (match chars
          ;; we're done, move to next line
          [()
           ($ text-buffer 'crlf)
           (bcom (next-line-handler rest-lines))]
          ;; otherwise add this character
          [(this-char . rest-chars)
           ($ text-buffer 'insert-char this-char %GREEN-N)
           (bcom (next rest-chars))])]))
    (next))

  (define (brief-pause rest-lines)
    (define type-pause
      (spawn ^loopdown 20))
    (extend-methods base-methods
     [(tick)
      ($ type-pause 'sub1)
      (when ($ type-pause 'zero?)
        ($ text-buffer 'crlf)
        (bcom (next-line-handler rest-lines)))]))

  (next-line-handler (credits-lines)))

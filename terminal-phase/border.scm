(define-module (terminal-phase border)
  #:use-module (ncurses curses)
  #:export (blit-horizontal-line
            blit-border))

;; ┌────────┐
;; │        │
;; │        │
;; │        │
;; └────────┘

(define* (blit-horizontal-line screen
                               left-char middle-char right-char
                               width
                               #:key (x 0) (y 0))
  (when (< width 2)
    (error "Line width too short:" width))
  ;; leftmost
  (addch screen left-char #:x x #:y y)
  (do ((i (1+ x) (1+ i))
       (remaining (- width 2) (1- remaining)))
      ((zero? remaining))
    (addch screen middle-char #:x i #:y y))
  (addch screen right-char #:x (+ x width -1) #:y y))

(define* (blit-border screen #:key (x 0) (y 0)
                      (width (- (getmaxx screen) x)) (height (- (getmaxy screen) y))
                      (ul (acs-ulcorner))
                      (ur (acs-urcorner))
                      (ll (acs-llcorner))
                      (lr (acs-lrcorner))
                      (hl (acs-hline))
                      (vl (acs-vline)))
  (when (< height 2)
    (error "Border height too short:" height))
  ;; blit top row
  (blit-horizontal-line screen
                        ul hl ur width
                        #:x x #:y y)
  (let ((right-x (+ x width -1)))
    (do ((i (1+ y) (1+ i))
         (remaining (- height 2) (1- remaining)))
        ((zero? remaining))
      (addch screen vl #:x x #:y i)
      (addch screen vl #:x right-x #:y i)))
  ;; blit bottom row
  (blit-horizontal-line screen
                        ll hl lr width
                        #:x x #:y (+ y height -1)))

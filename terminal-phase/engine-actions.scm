(define-module (terminal-phase engine-actions)
  #:export (push-engine-action!
            get-and-clear-engine-actions!
            apply-all-engine-actions!))

(define %engine-actions '())

(define (push-engine-action! action)
  (set! %engine-actions
        (cons action %engine-actions)))

(define (get-and-clear-engine-actions!)
  (define _engine-actions
    %engine-actions)
  (set! %engine-actions '())
  _engine-actions)

(define (apply-all-engine-actions! am)
  (define engine-actions (get-and-clear-engine-actions!))
  (for-each (lambda (action)
              (action am))
            engine-actions))

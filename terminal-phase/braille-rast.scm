(define-module (terminal-phase braille-rast)
  #:use-module (rnrs arithmetic fixnums)
  #:export (brailleify-text
            brailleify-text/list
            brailleify-grid/list))

; 0 3
; 1 4
; 2 5
; 6 7
(define (braille-configuration . bits)
  (let loop ((bits bits)
             (bit 1)
             (rbits 0))
    (if (null? bits)
        (integer->char (fx+ #x2800 rbits))
        (loop (cdr bits)
              (fxarithmetic-shift-left bit 1)
              (if (car bits)
                  (fxior rbits bit)
                  rbits)))))

(define (brailleify-grid/list grid)
  (define grid-height
    (vector-length grid))
  (define grid-width
    (vector-length (vector-ref grid 0)))

  (define (grid-ref x y)
    (if (>= y grid-height)
        #f
        (if (>= x grid-width)
            #f
            (and (vector-ref (vector-ref grid y) x) 'SS))))
  (define (configuration-at x y)
    (braille-configuration
     (grid-ref x y)
     (grid-ref x (+ y 1))
     (grid-ref x (+ y 2))
     (grid-ref (1+ x) y)
     (grid-ref (1+ x) (+ y 1))
     (grid-ref (1+ x) (+ y 2))
     (grid-ref x (+ y 3))
     (grid-ref (1+ x) (+ y 3))))
  
  (define (y-loop top-y)
    (define (x-loop left-x)
      (cond
       ((>= left-x grid-width)
        '())
       (else
        (cons (configuration-at left-x top-y)
              (x-loop (+ left-x 2))))))
    (cond
     ((>= top-y grid-height)
      '())
     (else
      (cons (list->string (x-loop 0))
            (y-loop (+ top-y 4))))))
  (y-loop 0))

;; Split text into a grid of vectors of vectors.
(define (vectorize-text text)
  (define split-text
    (string-split text #\newline))
  (define longest-line
    (apply max (map string-length split-text)))
  (define line-vectors
    (make-vector (length split-text)))
  (do ((lines split-text (cdr lines))
       (line-i 0 (1+ line-i)))
      ((null? lines))
    (let* ((line (car lines))
           (line-len (string-length line))
           (line-vec (make-vector longest-line #f)))
      (do ((char-i 0 (1+ char-i)))
          ((= char-i line-len))
        (let ((char (string-ref line char-i)))
          (unless (eq? char #\space)
            (vector-set! line-vec char-i 'SS))))
      (vector-set! line-vectors line-i line-vec)))
  line-vectors)

(define (brailleify-text/list text)
  (define vectext
    (vectorize-text text))
  (brailleify-grid/list vectext))

(define (brailleify-text text)
  (string-join (brailleify-text/list text) "\n"))


#;(module+ main
  (define everything-is-fine-periods
    "\
 .... .   . .... ...  .   .
 .    .   . .    .  . .   .
 ...  .   . ...  ...   ...
 .     . .  .    .  .   .
 ....   .   .... .  .   .

  ..... .  . . .   .  ...
    .   .  . . ..  . .
    .   .... . . . . . ...
    .   .  . . .  .. .   .
    .   .  . . .   .  ...

            ......
          .........
        .....    ....
       ...         ...
      ..   ..   ..   ..
      ..   ..   ..   ..
      ..             ..
      .. .         . ..
      ..  ...   ...  ..
       ...   ...   ...
        .....   .....
          .........
            .....

           .   ...
           .  .
           .   ...
           .      .
           .  ....

      .... . .   . ....
      .    . ..  . .
      ...  . . . . ...
      .    . .  .. .
      .    . .   . ....")

  (display (brailleify-text everything-is-fine-periods))
  (newline))


(define-module (terminal-phase level-tape)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-1)   ; list utils
  #:use-module (srfi srfi-41)  ; streams
  #:use-module (srfi srfi-43)  ; vector utils
  #:use-module (goblins)
  #:use-module (terminal-phase namespace-env)
  #:export (read-level-tape
            open-level-tape ^tape-eater
            ^lazy-tape-eater
            list->tape-eaters
            level-tape-env))

;; Separate level text into flavor lines, tile lines, and count
;; of longest tile line
(define (read-level-tape level-text)
  (define text-lines
    (list->vector (string-split (string-trim-right level-text #\newline)
                                #\newline)))
  (define split-line-nums
    (let lp ([lines (vector->list text-lines)]
             [count 0])
      (match lines
        ['() '()]
        [(this-line rest-lines ...)
         (if (and (> (string-length this-line) 0)
                  (eq? (string-ref this-line 0) #\-))
             (cons count (lp rest-lines (1+ count)))
             (lp rest-lines (1+ count)))])))

  (define-values (flavor-lines tile-lines level-instruction-lines)
    (match split-line-nums
      ['()
       (values (vector) text-lines (vector))]
      [(first-split)
       (values (vector-copy text-lines 0 first-split)
               (vector-copy text-lines (1+ first-split))
               (vector))]
      [(first-split second-split)
       (values (vector-copy text-lines 0 first-split)
               (vector-copy text-lines (1+ first-split) second-split)
               (vector-copy text-lines (1+ second-split)))]))
  (define longest-tile-line
    (apply max (map string-length (vector->list tile-lines))))

  (define (flavor-tiles flavors tiles)
    (if (or (null? flavors)
            (null? tiles))
        ;; we've reached the end of either
        ;; so... that's it... the rest aren't flavor.
        (map (lambda (t)
               (cons t #f))
             tiles)
        (match-let ([(flavor rest-flavors ...)
                     flavors]
                    [(tile rest-tiles ...)
                     tiles])
          ;; Now we need to see if this is an "interesting tile"
          (match tile
            [(or #\space #f)
             ;; boring!  just cons it on as #f and
             ;; loop to the next one then
             (cons #f
                   (flavor-tiles flavors rest-tiles))]
            [_
             ;; ok it's an interesting tile.
             (cons (cons tile
                         (match flavor
                           [#\space #f]
                           [_ flavor]))
                   (flavor-tiles rest-flavors
                                 rest-tiles))]))))


  (define (strip-spaces cells)
    (remove (lambda (x) (eq? x #\space)) cells))

  (define (col-ref i)
    (define (line-cells lines)
      (define num-lines (vector-length lines))
      (let lp ((lnum 0))
        (if (= lnum num-lines)
            '()
            (let* ((line (vector-ref lines lnum)))
              (cons (and (< i (string-length line))
                         (string-ref line i))
                    (lp (1+ lnum)))))))
    (define flavor-cells
      (line-cells flavor-lines))
    (define tile-cells
      (line-cells tile-lines))
    (define level-instruction-cells
      (line-cells level-instruction-lines))
    (vector (flavor-tiles flavor-cells tile-cells)
            (strip-spaces level-instruction-cells)))

  ;; Build up level information
  (let lp ([i 0])
    (if (= i longest-tile-line)
        '()
        (cons (col-ref i)
              (lp (1+ i))))))

(define (open-level-tape level-filename)
  (read-level-tape
   (call-with-input-file level-filename
     get-string-all)))


;; An actor that "eats" the tape one column at a time, until
;; it reaches the end and returns #f
(define (^tape-eater bcom level-tape)
  (lambda ()
    (if (null? level-tape)
        (vector #f #f)
        (bcom (^tape-eater bcom
                           (cdr level-tape))
              (car level-tape)))))

;; A far too clever actor, admittedly.  But I wanted to see if I could
;; make something that's lazy about loading the file and works with
;; Aurie and keeps internal state but is very smart about how it's
;; re-instantiated.
;; And this does it!  It doesn't load the file until it needs to,
;; and then it resumes at the appropriate place.  And it only stores
;; the filename and the position... that's it!
(define* (^lazy-tape-eater bcom filename #:optional [pos 0])
  ;; Initially, we don't load any level data in at all... only once the
  ;; actor is actually invoked.
  (define (pre-load-beh)
    (define tape-eater
      (spawn ^tape-eater (open-level-tape filename)))
    (define (make-loaded-beh pos)
      (define (loaded-beh)
        (bcom (make-loaded-beh (1+ pos))
              ($ tape-eater)))
      (define (loaded-self-portrait)
        (list filename pos))
      (portraitize loaded-beh loaded-self-portrait))
    ;; Fast forward, if appropriate.
    (do ((i 0 (1+ i)))
        ((= i pos))
      ($ tape-eater))
    ;; Now switch to loaded mode and return the value of calling tape-eater
    ;; once
    (bcom (make-loaded-beh (1+ pos))
          ($ tape-eater)))
  (define (pre-load-self-portrait)
    (list filename pos))
  (portraitize pre-load-beh pre-load-self-portrait))

(define (list->tape-eaters filenames)
  (map (lambda (filename)
         (spawn ^lazy-tape-eater filename))
       filenames))

(define-namespace-env level-tape-env
  (terminal-phase level-tape)
  ^lazy-tape-eater)

;; (module+ test
;;   (require goblins
;;            rackunit)
;;   (define am (make-actormap))
;;   (define test-level-tape
;;     (read-level-tape "\
;; 1
;; 23
;;   4
;; --
;; a e
;; bc
;;   f
;;  d
;;   g"))
;;
;;   (define tape-eater
;;     (actormap-spawn! am ^tape-eater test-level-tape))
;;
;;   (check-equal?
;;    (actormap-poke! am tape-eater)
;;    '#(((#\a . #\1)
;;        (#\b . #\2)
;;        #f
;;        #f
;;        #f)
;;       ()))
;;   (check-equal?
;;    (actormap-poke! am tape-eater)
;;    '#((#f
;;        (#\c . #f)
;;        #f
;;        (#\d . #\3)
;;        #f)
;;       ()))
;;   (check-equal?
;;    (actormap-poke! am tape-eater)
;;    '#(((#\e . #f)
;;        #f
;;        (#\f . #f)
;;        #f
;;        (#\g . #\4))
;;       ()))
;;   (check-equal?
;;    (actormap-poke! am tape-eater)
;;    #f))

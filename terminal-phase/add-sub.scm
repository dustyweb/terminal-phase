(define-module (terminal-phase add-sub)
  #:use-module (goblins)
  #:export (cell-add1
            cell-sub1))

(define _void (if #f #f))

(define (cell-add1 cell)
  ($ cell (1+ ($ cell)))
  _void)

(define (cell-sub1 cell)
  ($ cell (1- ($ cell)))
  _void)

(define-module (terminal-phase say-it)
  #:use-module (terminal-phase border)
  #:use-module (ncurses curses)
  #:export (say-it-to-screen))

;; This assumes we've already rendered to the screen
(define* (say-it-to-screen screen say-this screen-width screen-height
                           #:key (blank-background? #t))
  "Say SAY-THIS to SCREEN which has a subscreen of SCREEN-WIDTH and SCREEN-HEIGHT"
  (define screen-half-width
    (floor-quotient screen-width 2))
  (define screen-half-height
    (floor-quotient screen-height 2))

  (define-values (text-width text-height)
    (if (string? say-this)
        (values (string-length say-this) 1)
        (values (apply max (map string-length say-this))
                (length say-this))))
  (define-values (box-width box-height)
    (values (+ text-width 2)
            (+ text-height 4)))
  (define-values (place-box-at-x place-box-at-y)
    (values (max (+ (- screen-half-width
                       (floor-quotient box-width 2))
                    1)
                 0)
            (max (+ (- screen-half-height
                       (floor-quotient box-height 2))
                    2)
                 0)))
  (define blank-line (make-string text-width #\space))
  ;; blank and after box text
  (when blank-background?
    (addstr screen blank-line
            #:x (1+ place-box-at-x) #:y (+ place-box-at-y 1))
    (addstr screen blank-line
            #:x (1+ place-box-at-x) #:y (+ place-box-at-y 2 text-height)))
  ;; Blit each line of text
  (do ((lines (if (string? say-this)   ; maybe listify say-this
                  (list say-this)
                  say-this)
              (cdr lines))
       (y (+ place-box-at-y 2) (1+ y)))
      ((null? lines))
    (when blank-background?
      (addstr screen blank-line
              #:x (+ place-box-at-x 2) #:y y))
    ;; Blit text
    (let* ((line (car lines))
           (line-width (string-length line))
           (x (+ place-box-at-x
                 (floor-quotient (- box-width line-width)
                                 2))))
      (addstr screen line
              #:x x #:y y)))
  ;; Blit the border
  (blit-border screen
               #:width box-width
               #:height box-height
               #:x place-box-at-x
               #:y place-box-at-y))

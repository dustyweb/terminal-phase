(define-module (terminal-phase paths)
  #:use-module (terminal-phase install-paths)
  #:export (get-levels-dir
            get-extra-dir
            ensure-save-dir!
            get-save-dir
            get-save-file-path
            save-file-exists?))

(define (get-levels-dir)
  (cond
   ((getenv "TERMINAL_PHASE_ASSETSDIR")
    => (lambda (assets-dir)
         (string-append assets-dir file-name-separator-string "levels")))
   (else levels-dir)))

(define (get-extra-dir)
  (cond
   ((getenv "TERMINAL_PHASE_ASSETSDIR")
    => (lambda (assets-dir)
         (string-append assets-dir file-name-separator-string "extra")))
   (else extra-dir)))

(define (home-local-dir)
  (string-append (getenv "HOME")
                 file-name-separator-string
                 ".local"))
(define (home-local-share-dir)
  (string-append (home-local-dir) file-name-separator-string
                 "share"))
(define (get-save-dir)
  (string-append (home-local-share-dir) file-name-separator-string
                 "terminal-phase"))

(define (ensure-save-dir!)
  (for-each (lambda (dir)
              (unless (file-exists? dir)
                (mkdir dir)))
            (list (home-local-dir)
                  (home-local-share-dir)
                  (get-save-dir))))

(define (get-save-file-path)
  (string-append (get-save-dir)
                 file-name-separator-string
                 "terminal-phase-save.syrup"))

(define (save-file-exists?)
  (file-exists? (get-save-file-path)))

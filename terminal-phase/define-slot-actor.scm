(define-module (terminal-phase define-slot-actor)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (goblins)
  #:export (define-slot-actor))

#;(define-syntax-rule (define-slot-actor (name bcom args ...)
                      (name*
                       setup-body ...
                       (slots ...))
                      body ...)
  (begin
    (define-actor (name* bcom slots ...)
      body ...)
    (define-actor (name bcom args ...)
      #:portrait (lambda () (list slots ...))
      #:restore (lambda (_version slots ...)
                  (spawn name* slots ...))
      setup-body ...
      (name* bcom slots ...))))

;; same api but simpler
(define-syntax-rule (define-slot-actor (name bcom args ...)
                      (name*
                       setup-body ...
                       (slots ...))
                      body ...)
  (begin
    (define-actor (name* bcom slots ...)
      #:frozen
      body ...)
    (define* (name bcom args ...)
      setup-body ...
      (spawn name* slots ...))))

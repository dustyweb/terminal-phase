(define-module (terminal-phase logo)
  #:export (logo-text
            logo-lines
            logo-width
            logo-height))

(define logo-text
  "\
  ______ _____ ______  _  _    _____ _   __ __    __
 /_##__//#___//##>##/ /#|/#|  /_#__//#| /#//##|  /#/
  /#/  /#/_  /##.--' /#####|   //  /##|/#//#<#| /#/
 /#/  /#__/ /#/\\#\\  /#/|/|#| _//_ /#/|##//#_##|/#/_
/_/  /____//_/  \\_\\/_/   |_|/___//_/ |_//_/ |_/___/

            ****  *  *  ****  ****  ****
           *  *  *  *  *  *  **    ***
          ****  ****  *****    ** *
         *     *  *  *   *  **** ****")

(define logo-lines
  (string-split logo-text #\newline))

(define logo-width
  (apply max (map string-length logo-lines)))

(define logo-height
  (length logo-lines))

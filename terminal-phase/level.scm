(define-module (terminal-phase level)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (ice-9 streams)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 control)
  #:use-module (srfi srfi-1)     ; list utils
  #:use-module (srfi srfi-43)    ; vector utils
  #:use-module (srfi srfi-41)    ; streams
  #:use-module (ncurses curses)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib ticker)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib selfish-spawn)
  #:use-module (goblins persistence-store syrup)
  #:use-module (terminal-phase border)
  #:use-module (terminal-phase credits)
  #:use-module (terminal-phase define-slot-actor)
  #:use-module (terminal-phase engine-actions)
  #:use-module (terminal-phase globals)
  #:use-module (terminal-phase level-const)
  #:use-module (terminal-phase level-tape)
  #:use-module (terminal-phase namespace-env)
  #:use-module (terminal-phase ncurses-stuff)
  #:use-module (terminal-phase no-op)
  #:use-module (terminal-phase paths)
  #:use-module (terminal-phase posinfo)
  #:use-module (terminal-phase say-it)
  #:use-module (terminal-phase sprites)
  #:use-module (terminal-phase score)           ; just to get score-env, might move
  #:use-module (terminal-phase starfield)
  #:export (^level
            ^level-manager
            level-env))

(define render-layer-order
  '(powerup
    explosion
    terrain
    enemy-bullet
    player-bullet
    enemy
    player))

(define (_layer-posinfos! pinfos layers)
  (match pinfos
    ('() 'done)
    ;; handle one posinfo
    ((? posinfo? pinfo)
     (define layer-name (posinfo-layer pinfo))
     (define cur-members (hashq-ref layers layer-name '()))
     ;; add member to current set
     (hashq-set! layers layer-name
                 (cons pinfo cur-members)))
    ;; unpack a list of posinfos
    ((posinfos ...)
     (for-each (lambda (pinfo)
                 (_layer-posinfos! pinfo layers))
               pinfos))))

(define (layer-posinfos pinfos)
  (define layers (make-hash-table))
  (_layer-posinfos! pinfos layers)
  layers)

(define (blit-posinfo-to-screen! pinfo screen)
  (define-values (x y char picolor layer bg-color)
    (values (posinfo-x pinfo)
            (posinfo-y pinfo)
            (posinfo-char pinfo)
            (posinfo-color pinfo)
            (posinfo-layer pinfo)
            (posinfo-bg-color pinfo)))
  (addch screen (color (match picolor
                         ((or 'white 'brwhite) %WHITE-N)
                         ((or 'red 'brred) %RED-N)
                         ((or 'yellow 'bryellow) %YELLOW-N)
                         ((or 'cyan 'brcyan) %CYAN-N)
                         ((or 'magenta 'brmagenta) %MAGENTA-N)
                         ((or 'green 'brgreen) %GREEN-N)
                         ((or 'blue 'brblue) %BLUE-N)
                         (_ %WHITE-N))
                       (match picolor
                         ((or 'brwhite 'brred 'bryellow 'brcyan
                              'brmagenta 'brgreen 'brblue)
                          (bold char))
                         (_ (normal char))))
         #:x (+ x 1)
         #:y (+ y 2)))

(define (make-empty-terrain-vector)
  (let ([empty-col
         (make-vector LEVEL-HEIGHT #f)])
    (make-vector LEVEL-WIDTH empty-col)))

(define empty-terrain-vector
  (make-empty-terrain-vector))

(define (terrain-push-col terrain-vec col-vec)
  (define new-terrain
    (make-empty-terrain-vector))

  ;; copy over all terrain except first element, shifted over one
  (vector-copy! new-terrain 0
                terrain-vec 1)

  ;; blit in the new column
  (vector-set! new-terrain (1- LEVEL-WIDTH) col-vec)

  new-terrain)

(define-actor (^terrain bcom #:optional [terrain-vec empty-terrain-vector])
  (define (terrain-ref x y)
    (vector-ref (vector-ref terrain-vec x) y))
  (define terrain-len (vector-length terrain-vec))

  (define (make-terrain-posinfo x y)
    (posinfo x y
             #\# 'red
             'terrain #f))

  ;;;; The original Racket more readable version:
  ;; (for/fold ([posinfos '()])
  ;;           ([col terrain-vec]
  ;;            [x (in-naturals)])
  ;;   (for/fold ([posinfos posinfos])
  ;;             ([cell col]
  ;;              [y (in-naturals)])
  ;;     (if (terrain-ref x y)
  ;;         (cons (posinfo x y
  ;;                        #\# 'red
  ;;                        'terrain #f)
  ;;               posinfos)
  ;;         posinfos))))
  (define posinfos
    (fold
     (lambda (x posinfos)
       (define col (vector-ref terrain-vec x))
       (fold
        (lambda (y posinfos)
          (define cell (vector-ref col y))
          (if (terrain-ref x y)
              (cons (make-terrain-posinfo x y)
                    posinfos)
              posinfos))
        posinfos
        (iota (vector-length col))))
     '()
     (iota (vector-length terrain-vec))))

  (methods
   [tick no-op]
   ;; Push a column of terrain (a vector of... booleans?)
   ;; onto the terrain-vec
   [(push-col col-vec)
    (bcom (^terrain bcom (terrain-push-col terrain-vec col-vec)))]
   ;; Write out the full terrain as a list
   [(posinfo)
    posinfos]
   ;; We're not the ones affected by a collision... the object
   ;; colliding is!
   [collide no-op]
   ;; TODO: I guess maybe the level advancement code goes... here?
   ;;   Not sure.
   [pre-level-advance no-op]
   [post-level-advance no-op]))

(define terrain-speed-control-chars
  (alist->hashq-table
   '((#\0 . 0)
     (#\1 . 1)
     (#\2 . 2)
     (#\3 . 3)
     (#\4 . 4)
     (#\5 . 5)
     (#\6 . 6)
     (#\7 . 7)
     (#\8 . 8)
     (#\9 . 9)
     (#\! . 10)
     (#\@ . 12)
     (#\# . 14)
     (#\$ . 16)
     (#\% . 18)
     (#\^ . 20)
     (#\& . 22)
     (#\& . 24)
     (#\* . 26)
     (#\( . 28)
     (#\) . 30))))

(define (terrain-speed-char? char)
  (hashq-ref terrain-speed-control-chars char))

(define-syntax-rule (for ((item lst)) body ...)
  (for-each (lambda (item) body ...) lst))

(define level-banner
  ".-*[ TERMINAL PHASE ]*-.")

(define where-to-put-level-banner
  (- (/ LEVEL-WIDTH 2)
     (/ (string-length level-banner) 2)
     -1))

(define (posinfo->vector-pos this-posinfo)
  (+ (* LEVEL-HEIGHT (posinfo-x this-posinfo))
     (posinfo-y this-posinfo)))

;; This shares some common state between the level and the custom ticky
;; so the ticky can easily unpack without too many extra arguments.
(define-actor (^level-stateling bcom score-tracker
                                last-layout player-x player-y
                                ticked-this-turn? terrain-speed)
  (define state-as-list
    (list score-tracker
          last-layout player-x player-y
          ticked-this-turn? terrain-speed))
  (lambda () state-as-list))

;; Really, this is the interface between each ticked game object and the
;; underlying engine.  So we extend the ticky a bit with some extra stuff.
;;
;; We add some extra methods to this ticky that let this object
;; do some things:
;;  - Add points to the score on death
(define-actor (^custom-ticky bcom orig-ticky this-obj-box
                             level-stateling   ; internal level state stuff
                             #:key [points #f])
  (define (default-other-on-collide with this-posinfo phase)
    ($ with 'collide ($ this-obj-box) this-posinfo phase))
  ;; We do this so that restoration code works right, since we can't call
  ;; $ during rehydration.
  (define (pre-setup-beh . args)
    (define default-beh (make-default-beh))
    (bcom default-beh
          (apply default-beh args)))
  (define (make-default-beh)
    ;; Unpack the state internal to the level.
    (define-values (score-tracker
                    last-layout player-x player-y
                    ticked-this-turn? terrain-speed)
      (apply values ($ level-stateling)))
    (extend-methods orig-ticky
      ;; Give points for death via the ticky
      [(die #:key [points? #f])
       (when (and points points?)
         ($ score-tracker 'add points))
       ($ orig-ticky 'die)]
      ;; Check if we're out of bounds
      ;; (This will change with possibly changing level sizes in the future)
      [(oob? x y)
       (oob? x y)]
      ;; Collides with the last layout?
      [(last-collide check-posinfos phase collide-this
                     #:key
                     [collide-other default-other-on-collide])
       (define this-obj
         ($ this-obj-box))
       ;; Who else is here?
       (for-each
        (lambda (check-posinfo)
          (unless (oob? (posinfo-x check-posinfo)
                        (posinfo-y check-posinfo))
            (let ((vector-pos
                   (posinfo->vector-pos check-posinfo)))
              (for-each (lambda (co-inhabitant)
                          (match co-inhabitant
                            (#(other-ticked other-posinfo)
                             (unless (eq? other-ticked this-obj)
                               (collide-this other-ticked other-posinfo phase)
                               (collide-other other-ticked check-posinfo phase)))))
                        (vector-ref ($ last-layout) vector-pos)))))
        (match check-posinfos
          [(items ...)
           items]
          [posinfo (list posinfo)]))]

      ;; TODO: we're adding keyword arguments... we might want to accept at
      ;;   this point that this should be named something else?  Dunno.
      ;; TODO: I'm lost about what to do about this-obj-box
      [(to-tick give-ticky #:key [points 0])
       ($ orig-ticky 'to-tick
          (λ (ticky)
            (define-cell new-obj-box)
            (define customized-ticky
              (spawn ^custom-ticky ticky new-obj-box
                     level-stateling))
            (define new-refr
              (give-ticky customized-ticky))
            ($ new-obj-box new-refr)
            new-refr))]

      [(player-pos)
       (cons ($ player-x) ($ player-y))]

      [(ticked-this-turn?)
       ($ ticked-this-turn?)]

      [(terrain-speed)
       ($ terrain-speed)]))
  pre-setup-beh)

;; Calculate collisions and inform participants.
;; The big, general collision detection.
(define (do-collisions level-ticker last-layout)
  (define level-vector
    (make-vector (* LEVEL-WIDTH LEVEL-HEIGHT) '()))
  ;; Fill vector
  ($ level-ticker 'foldr
     (lambda (ticked _)
       (define posinfos
         (match ($ ticked 'posinfo)
           [(items ...)
            items]
           [posinfo (list posinfo)]))
       (for-each
        (lambda (this-posinfo)
          (define vector-pos
            (posinfo->vector-pos this-posinfo))
          (vector-set! level-vector vector-pos
                       (cons (vector ticked this-posinfo)
                             (vector-ref level-vector vector-pos))))
        posinfos))
     #f)
  ($ last-layout level-vector)
  ;; Go through and inform of collisions
  (do ((i 0 (1+ i)))
      ((= i (vector-length level-vector)))
    (let ((co-habitants (vector-ref level-vector i)))
      (match co-habitants
        ;; zero or more co-habitants is no real collision
        ['()
         'no-op]
        [(one-entity)
         'no-op]
        ;; ok, inform colliders
        [_
         (define informed-collisions
           (make-hash-table))
         (for ([this-inhabitant co-habitants])
              (for ([other-inhabitant co-habitants])
                   ;; skip if the same
                   (unless (eq? this-inhabitant other-inhabitant)
                     ;; otherwise, inform me
                     (match-let* ((#(this-ticked this-posinfo)
                                   this-inhabitant)
                                  (#(other-ticked other-posinfo)
                                   other-inhabitant)
                                  ;; Set of objects this-ticked has already collided with
                                  (already-collided
                                   (or (hashq-ref informed-collisions this-ticked)
                                       (let ([new-collided (make-hash-table)])
                                         (hashq-set! informed-collisions this-ticked
                                                     new-collided)
                                         new-collided))))
                       (unless (hashq-ref already-collided other-ticked)
                         ($ this-ticked 'collide
                            other-ticked other-posinfo 'super-collide)
                         (hashq-set! already-collided other-ticked #t))))))]))))

(define* (advance-level tape-eater terrain-speed
                        level-ticker terrain level-stateling
                        #:optional [level-right-x (1- LEVEL-WIDTH)])
  (call/ec
   (lambda (return)
     (define cells-column-and-level-instructions
       ($ tape-eater))
     (define cells-column
       (vector-ref cells-column-and-level-instructions 0))
     (define level-instructions
       (vector-ref cells-column-and-level-instructions 1))

     (unless cells-column
       (return 'level-over))

     ;; Process any level instructions.
     (for ([level-instruction level-instructions])
          (match level-instruction
            [(? terrain-speed-char?)
             (let ((new-speed
                    (hashq-ref terrain-speed-control-chars
                               level-instruction)))
               ($ terrain-speed new-speed))]
            [_ 'no-op]))

     ($ level-ticker 'tick 'pre-level-advance)

     ;; Now we walk through the column and build up the new
     ;; terrain-col
     (define terrain-col
       (make-vector (length cells-column) #f))
     (let lp ((y 0)
              (cells cells-column))
       (match cells
         ('() 'done)
         ((cell rest-cells ...)
          (match cell
            [(#\# . _)
             (vector-set! terrain-col y #t)]
            [(tile-char . flavor-char)
             ;; Here's how we spawn a ticked enemy.
             ;;
             ;; NOTE: For whatever reason the enemies don't specify how many
             ;; points they're worth when they die... instead, they get given
             ;; a point by whatever constructs them.  I think I was
             ;; experimenting with some vague PoLA reason for this but it
             ;; doesn't make much sense in retrospect.
             ;;
             ;; A better design would be to give the user points through the
             ;; ticky.  If you really want to do some PoLA stuff you could limit
             ;; the number of permitted points that something, or its children,
             ;; can give.
             (define (spawn-ticked-enemy constructor points)
               ($ level-ticker 'to-tick
                  (λ (ticky)
                    (define-cell obj-box #f)
                    (define score-ticky
                      (spawn ^custom-ticky ticky obj-box
                             level-stateling
                             #:points points))
                    (define enemy
                      (spawn constructor score-ticky level-right-x y))
                    ($ obj-box enemy)
                    enemy)))
             ;; Here's all of the level characters -> enemy spawners we've
             ;; written so far
             (match tile-char
               [#\<
                (match flavor-char
                  [#\f (spawn-ticked-enemy ^enemy:<-firing 10)]
                  [#\F (spawn-ticked-enemy ^enemy:<-firing-collides 10)]
                  ;; default
                  [_ (spawn-ticked-enemy ^enemy:< 10)])]
               [#\S
                (match flavor-char
                  [#\s
                   (spawn-ticked-enemy ^enemy:slightly-slow-squiggler 15)]
                  [_ (spawn-ticked-enemy ^enemy:squiggler 15)])]
               [#\n
                (spawn-ticked-enemy ^enemy:nymph 20)]
               [#\K
                (spawn-ticked-enemy ^enemy:kamikaze 20)]
               [#\I
                (spawn-ticked-enemy ^enemy:turret 40)]
               [#\+
                (spawn-ticked-enemy ^enemy:mine 10)]
               [#\^
                (spawn-ticked-enemy ^enemy:up-cannon 40)]
               [_ 'no-op])]
            [#f 'no-op])
          (lp (1+ y) rest-cells))))

     ($ terrain 'push-col terrain-col)

     ;; Inform all ticked items that the level has advanced
     ($ level-ticker 'tick 'post-level-advance))))


(define-slot-actor (^level bcom tape-eater score-tracker lives level-count)
  (^level*
   (define-cell ticked-this-turn?
     #f)

   (define-cell terrain-speed
     default-terrain-speed)

   (define level-ticker (spawn-ticker))

   (define-cell exit-reason #f)

   (define-cell player-x 1)
   (define-cell player-y
     (floor-quotient LEVEL-HEIGHT 2))

   ;; TODO: Store last layout when generating the level
   (define-cell last-layout)

   (define level-stateling
     (spawn ^level-stateling score-tracker last-layout
            player-x player-y
            ticked-this-turn? terrain-speed))

   ;; Define the player and spawn a ticked facet of it
   ;; TODO: Yet another reduntant way of setting up the custom-ticky...
   (define player-ship
     ($ level-ticker 'to-tick
        (lambda (ticky)
          (define-cell new-obj-box #f)
          (define customized-ticky
            (spawn ^custom-ticky ticky new-obj-box level-stateling))
          (define player-refr
            (spawn ^player-ship customized-ticky lives player-x player-y))
          ($ new-obj-box player-refr)
          player-refr)))

   (define terrain
     ($ level-ticker 'to-tick
        (lambda (ticky)
          (spawn ^terrain))))

   ;; Fast forward the level to its starting position
   (do ([i 0 (1+ i)])
       ((= LEVEL-WIDTH i))
     (advance-level tape-eater terrain-speed
                    level-ticker terrain level-stateling
                    i))

   ;; Do a first path of running collisions
   ;; (hopefully there aren't any immediately!)
   (do-collisions level-ticker last-layout)

   ;; set by the (running) 
   (define-cell state `(running ,($ terrain-speed)))

   (score-tracker
    lives level-count tape-eater
    ticked-this-turn? terrain-speed
    level-ticker exit-reason
    player-x player-y last-layout player-ship
    terrain level-stateling
    state))

  ;; set up starfield, if appropriate
  (define starfield
    (and (%starfield?)
         (spawn ^starfield (* LEVEL-WIDTH 2) (* LEVEL-HEIGHT 4))))

  (when starfield
    (prime-starfield starfield (* LEVEL-WIDTH 2)))

  (define screen-width (+ LEVEL-WIDTH 4))
  (define screen-height (+ LEVEL-HEIGHT 6))
  ;; We need to reuse cons cells for wh so we can test if they've changed using
  ;; eq? to see if we should reconfigure %inlaid-screen
  (define screen-wh-pair (cons screen-width screen-height))

  ;; (define header
  ;;   (raart:fg 'brwhite
  ;;             (raart:text ".-*[ TERMINAL PHASE ]*-.")))
  ;; (define blank-level-canvas
  ;;   (raart:blank LEVEL-WIDTH LEVEL-HEIGHT))

  (define base-methods
    (methods
     [(exit-reason) ($ exit-reason)]
     [(screen-size) screen-wh-pair]
     [(render screen)
      (define ticked-posinfos
        (map (lambda (obj)
               ($ obj 'posinfo))
             ($ level-ticker 'get-ticked)))
      (define layers-to-blit
        (layer-posinfos ticked-posinfos))
      (define (blit-posinfo! pinfo)
        (blit-posinfo-to-screen! pinfo screen))
      (define (blit-layer! layer-name)
        (define posinfos
          (hashq-ref layers-to-blit layer-name '()))
        (for-each blit-posinfo! posinfos))
      ;; Blit out the banner
      (addstr screen level-banner #:x where-to-put-level-banner #:y 0)
      ;; Blit out the border
      (blit-border screen
                   #:width (+ LEVEL-WIDTH 2)
                   #:height (+ LEVEL-HEIGHT 2)
                   #:x 0 #:y 1)
      ;; Blit out the starfield
      (when starfield
        ($ starfield 'render screen
                     #:x-start 1
                     #:y-start 2))
      ;; now blit out all the layers, in order
      (for-each blit-layer! render-layer-order)

      ;; Finally the level, score, etc
      (let* ((row1 (+ 1 1 LEVEL-HEIGHT 1))
             (row2 (+ row1 1))
             (left-label-col 1)
             (left-val-col (+ left-label-col 7))
             (right-label-col (- LEVEL-WIDTH 17))
             (right-val-col (+ right-label-col 10)))
        (addchstr screen (bold "LIVES:")
                  #:x left-label-col #:y row1)
        (addstr screen (format #f "~3,'0d" ($ lives))
                #:x left-val-col #:y row1)
        (addchstr screen (bold "LEVEL:")
                  #:x left-label-col #:y row2)
        (addstr screen (format #f "~3,'0d" ($ level-count)) ; TODO
                #:x left-val-col #:y row2)
        (addchstr screen (bold "SCORE:")
                  #:x right-label-col #:y row1)
        (addstr screen (format #f "~8,'0d" ($ score-tracker 'get-score))
                #:x right-val-col #:y row1)
        (addchstr screen (bold "HI-SCORE:")
                  #:x right-label-col #:y row2)
        (addstr screen (format #f "~8,'0d" ($ score-tracker 'get-high-score))
                #:x right-val-col #:y row2))]

     ;; [(render)
     ;;  (define blit-layers
     ;;    ($ level-ticker 'foldr
     ;;       (lambda (ticked layers)
     ;;         (define (add-to-layers posinfo layers)
     ;;           (define layer
     ;;             (posinfo-layer posinfo))
     ;;           (define current-occupants
     ;;             (hash-ref layers layer '()))
     ;;           (hash-set layers layer
     ;;                     (cons posinfo current-occupants)))
     ;;         (match ($ ticked 'posinfo)
     ;;           ;; nothing to do
     ;;           [(or '() #f)
     ;;            layers]
     ;;           [(posinfos ...)
     ;;            (foldl add-to-layers layers posinfos)]
     ;;           [posinfo (add-to-layers posinfo layers)]))
     ;;       #hasheq()))

     ;;  ;; (define backdrop-canvas
     ;;  ;;   (if starfield?
     ;;  ;;       ($ starfield 'render)
     ;;  ;;       blank-level-canvas))

     ;;  (define level-canvas
     ;;    (for/fold ([canvas backdrop-canvas])
     ;;              ([layer render-layer-order])
     ;;      (for/fold ([canvas canvas])
     ;;                ([this-posinfo (hash-ref blit-layers layer '())])
     ;;        (match-define (posinfo x y char color layer-type bg-color)
     ;;          this-posinfo)
     ;;        (define colored-char
     ;;          (raart:fg color (raart:char char)))
     ;;        (define bg-char
     ;;          (if bg-color
     ;;              (raart:bg bg-color colored-char)
     ;;              colored-char))
     ;;        (raart:place-at canvas y x bg-char))))

     ;;  (define footer
     ;;    (raart:place-at
     ;;     (raart:place-at
     ;;      (raart:blank LEVEL-WIDTH 2)
     ;;      0 (- LEVEL-WIDTH 18) ; 18 is width of HI-SCORE + zeroes
     ;;      (raart:vappend
     ;;       #:halign 'right
     ;;       (raart-labeled-val "SCORE"
     ;;                          (~r ($ score-tracker 'get-score)
     ;;                              #:min-width 8 #:pad-string "0"))
     ;;       (raart-labeled-val "HI-SCORE"
     ;;                          (~r ($ score-tracker 'get-high-score)
     ;;                              #:min-width 8 #:pad-string "0"))))
     ;;     0 0
     ;;     (raart:vappend
     ;;      #:halign 'left
     ;;      (raart-labeled-val "LIVES" (number->string ($ lives))
     ;;                         #:goal-width 10)
     ;;      (raart-labeled-val "LEVEL" "1"
     ;;                         #:goal-width 10))))

     ;;  (raart:vappend
     ;;   #:halign 'center
     ;;   header
     ;;   (raart:frame level-canvas)
     ;;   footer)]

     ;; you'll need to implement handle-event though
     [tick no-op]
     ;; This one's for debugging
     [(get-level-ticker) level-ticker]))

  (define* (running #:optional [tape-countdown ($ terrain-speed)])
    ($ state `(running ,tape-countdown))
    (extend-methods base-methods
      [(tick)
       (when starfield
         ($ starfield 'advance ($ terrain-speed)))

       (call/ec
        (lambda (return)
          ;; maybe advance tape
          (cond
           [(zero? tape-countdown)
            ($ ticked-this-turn? #t)
            (let ((advance-result (advance-level tape-eater terrain-speed
                                                 level-ticker terrain
                                                 level-stateling)))
              ;; Level's over?  End it now by congratulating
              ;; the player!
              (when (eq? advance-result 'level-over)
                (return (bcom (you-win)))))]
           [else
            ($ ticked-this-turn? #f)])

          ;; Tick all the level items
          ($ level-ticker 'tick 'tick)

          ;; collision detection time
          (do-collisions level-ticker last-layout)

          (cond
           [($ player-ship 'dead?)
            (bcom (game-over))]
           [else
            ;; decrement/reset tape counter
            (bcom (running
                   (if (zero? tape-countdown)
                       ($ terrain-speed)
                       (1- tape-countdown))))])))]

      [(handle-event evt)
       (match evt
         [259   ; up
          ($ player-ship 'move-up)]
         [258   ; down
          ($ player-ship 'move-down)]
         [260   ; left
          ($ player-ship 'move-left)]
         [261   ; right
          ($ player-ship 'move-right)]
         [#\space
          ($ player-ship 'fire)]
         [(or #\q #\esc)
          ($ exit-reason 'quit)]
         [_
          'no-op])]))

  (define* (say-it-state say-this give-exit-reason)
    ($ state `(say-it ,say-this ,give-exit-reason))
    (extend-methods
     base-methods
     [(render screen)
      (base-methods 'render screen)
      (say-it-to-screen screen say-this LEVEL-WIDTH LEVEL-HEIGHT)]
     [(handle-event evt)
      (match evt
        [(or #\q #\space #\return #\newline)
         ($ exit-reason give-exit-reason)]
        [_ 'no-op])]))

  (define (game-over)
    (say-it-state ;; (raart:text " Game Over! ")
     " Game Over! " 'game-over))

  (define (you-win)
    (say-it-state " Success! " 'beat-level))

  ;; lazy re-instantiation of state
  (define (pre-setup-beh . args)
    (define beh
      ;; Setup or restore actor to its relevant state
      (match ($ state)
        (('running tape-countdown)
         (running tape-countdown))
        (('say-it say-this give-exit-reason)
         (say-it-state say-this give-exit-reason))))
    (bcom beh (apply beh args)))
  pre-setup-beh)

(define-actor (^level-manager* bcom selfy score-tracker
                               lives level-count state)
  (define (next-level-state levels)
    (if (null? levels)
        (victory)
        (play-next-level levels)))
  (define (play-next-level levels)
    (define level-tape-eater
      (car levels))
    (define level
      (spawn ^level level-tape-eater score-tracker lives level-count))
    (define next-levels
      (cdr levels))
    ($ level-count (1+ ($ level-count)))
    (playing level next-levels))
  (define (playing level next-levels)
    ($ state `(playing ,level ,next-levels))

    (define (maybe-handle-exit-method)
      (define exit-reason
        ($ level 'exit-reason))
      (match exit-reason
        [#f 'no-op]
        [(or 'quit 'game-over)
	 (set! %resize-screen? #t)
         ($ (%dpr-pushdown) 'pop)]
        ['beat-level
         (bcom (next-level-state next-levels))]))

    (extend-methods level
      [(level) level]
      [(tick)
       ;; tick the level first
       ($ level 'tick)
       (maybe-handle-exit-method)]
      [(handle-event evt)
       (match evt
         ;; intercept save key
         [#\S
          (push-engine-action!
           ;; Grab self outside of engine action because there won't be a
           ;; syscaller inside
           (let ((self ($ selfy)))
             (lambda (am)
               (ensure-save-dir!)
               (let ((syrup-store (make-syrup-store (get-save-file-path))))
                 (actormap-save-to-store! am level-env syrup-store self)))))
          ($ (%dpr-pushdown) 'pop)
          ($ (%dpr-pushdown) 'push
             (spawn ^say-saved level))]
         ;; otherwise pass it onto the level
         [_
          ($ level 'handle-event evt)
          (maybe-handle-exit-method)])]))
  (define (victory)
    ($ state `(victory))
    (methods
     [(render screen)
      (define end-text "\
          YOU WIN THE GAME

     Having fought might foes,
you held on strong and have emerged
    victorious! Congratulations!


  (... upcoming releases will have
            more levels!)")
      (define end-text-lines (string-split end-text #\newline))
      (define end-text-width (apply max (map string-length end-text-lines)))
      (define end-text-height (length end-text-lines))
      (define end-text-offset-x (+ (floor/ (- LEVEL-WIDTH end-text-width) 2) 2))
      (define end-text-offset-y (+ (floor/ (- LEVEL-HEIGHT end-text-height) 2) 2))
      (do ((lines end-text-lines (cdr lines))
	   (y end-text-offset-y (1+ y)))
	  ((null? lines))
	(addstr screen (car lines)
		#:x end-text-offset-x
		#:y y))
      ;; (raart:frame
      ;;  (raart:vappend
      ;;   #:halign 'center
      ;;   (raart:text " YOU WIN THE GAME ")
      ;;   (raart:text "")
      ;;   (raart:text " Having fought mighty foes, ")
      ;;   (raart:text " you held on strong and have emerged ")
      ;;   (raart:text " victorious!  Congratulations! ")
      ;;   (raart:text "")
      ;;   (raart:text "")
      ;;   (raart:text " (... upcoming releases will have ")
      ;;   (raart:text " more levels!) ")))
      ]
     [tick no-op]
     [(handle-event evt)
      (match evt
        [(or #\q #\space #\return #\newline)
         ($ (%dpr-pushdown) 'pop)
         ;; and now push on the credits
	 (set! %resize-screen? #t)
         ($ (%dpr-pushdown) 'push
            (spawn ^credits))]
        [_ 'no-op])]))
  ;; lazy re-instantiation of state
  (define (pre-setup-beh . args)
    (define beh
      ;; Setup or restore actor to its relevant state
      (match ($ state)
        (('play-next-level levels)
         (play-next-level levels))
        (('playing level next-levels)
         (playing level next-levels))
        (('victory) (victory))))
    (bcom beh (apply beh args)))
  pre-setup-beh)

;; This is just to tell the user that we saved before exiting.
(define (^say-saved bcom show-level)
  (extend-methods
   show-level
   [(render screen)
    ($ show-level 'render screen)
    (say-it-to-screen screen " Saved! " LEVEL-WIDTH LEVEL-HEIGHT)]
   [(handle-event evt)
    (match evt
      [(or #\q #\space #\return #\newline)
       ($ (%dpr-pushdown) 'pop)
       ;; and now push on the credits
       (set! %resize-screen? #t)]
      [_ 'no-op])]
   [(tick) 'no-op]))

(define (^level-manager bcom levels score-tracker)
  (define-cell lives
    3)
  (define-cell level-count
    0)
  (define-cell state
    `(play-next-level ,levels))
  (define-cell selfy #f)
  (define manager
    (spawn ^level-manager* selfy
           score-tracker lives level-count state))
  ($ selfy manager)
  manager)

(define-namespace-env level-env
  (terminal-phase level)
  ^terrain
  ^level-stateling
  ^custom-ticky
  (^level ^level*)
  (^level-manager ^level-manager*)
  #:extends (list cell-env sprites-env ticker-env score-env level-tape-env))

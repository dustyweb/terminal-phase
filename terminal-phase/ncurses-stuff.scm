(define-module (terminal-phase ncurses-stuff)
  #:use-module (ncurses curses)
  #:use-module (rnrs enums)
  #:export (%YELLOW-N
            %GREEN-N
            %WHITE-N
            %RED-N
            %CYAN-N
            %MAGENTA-N
            %BLUE-N

            install-colors!

            color-ref
            color-pair-ref))

;; Same order ncurses uses
(define colors
  '(black red green yellow blue magenta cyan white))
(define c-vec
  (vector COLOR_BLACK
          COLOR_RED
          COLOR_GREEN
          COLOR_YELLOW
          COLOR_BLUE
          COLOR_MAGENTA
          COLOR_CYAN
          COLOR_WHITE))

(define colors-len
  (length colors))
(define colors-enum
  (make-enumeration colors))

;; Get the id of a color (or a color against black)
(define color-ref
  (enum-set-indexer colors-enum))

(define (color-pair-ref fg-sym bg-sym)
  "Get the id of a color pair"
  (+ (color-ref fg-sym)
     (* (color-ref bg-sym) colors-len)))

(define %YELLOW-N (color-ref 'yellow))
(define %GREEN-N (color-ref 'green))
(define %WHITE-N (color-ref 'white))
(define %RED-N (color-ref 'red))
(define %CYAN-N (color-ref 'cyan))
(define %MAGENTA-N (color-ref 'magenta))
(define %BLUE-N (color-ref 'blue))

(define (do-init-pair! fg-color bg-color)
  (init-pair! (color-pair-ref fg-color bg-color)
              (vector-ref c-vec (color-ref fg-color))
              (vector-ref c-vec (color-ref bg-color))))

;; slow but whatevs, only done once
(define (install-colors!)
  "Install all colors into ncurses"
  (for-each (lambda (fg-color)
              (for-each (lambda (bg-color)
                          (do-init-pair! fg-color bg-color))
                        colors))
            colors))

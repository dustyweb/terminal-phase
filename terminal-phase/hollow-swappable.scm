(define-module (terminal-phase hollow-swappable)
  #:use-module (goblins)
  #:use-module (goblins actor-lib swappable)
  #:export (hollow-swappable))

(define (hollow-swappable name)
  "Makes a swappable actor that's empty at first"
  ;; Must be spawned with a refr, use a hollow one for now.
  ;; We expect the user to replace this later.
  (swappable (spawn (lambda _ (lambda _ 'noop)))
             name))
